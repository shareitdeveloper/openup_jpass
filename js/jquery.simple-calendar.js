// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;
(function($, window, document, undefined) {

    "use strict";

    // Create the defaults once
    var pluginName = "simpleCalendar",
        defaults = {
            months: ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'], //string of months starting from january
            days: ['sunday', 'monday', 'tuesday', 'wenesday', 'thursday', 'friday', 'saturday'], //string of days starting from sunday
            minDate: "YYYY-MM-DD", // minimum date
            maxDate: "YYYY-MM-DD", // maximum date
            insertEvent: true, // can insert events
            displayEvent: true, // display existing event
            fixedStartDay: true, // Week begin always by monday
            events: [], //List of event
            insertCallback: function() {} // Callback when an event is added to the calendar
        };



    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        if(this.settings.StartDate){
            this.currentDate = new Date(this.settings.StartDate);
         } else{
            this.currentDate = new Date();
         }
        this.events = options.events;

        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function() {
            var container = $(this.element);
         
            var todayDate = this.currentDate;
            var events = this.events;

            var calendar = $('<div class="calendar"></div>');
            // var header = $('<header>'+
            //                '<p class="m-0 month"></p>'+
            //                '<a class="btn " ><</a>'+
            //                '<a class="btn b" >></a>'+
            //                 '</header>');
            // var header = $('<header>'+
            //                 '<div class="row m-0 h-3p25rem  font-ffffff">'+
            //                     '<div class="col _2rem p-0 justify-content-center align-self-center">'+
            //                         '<a class="btn btn-prev px-0" ><</a>'+                             
            //                     '</div>'+                                  
            //                     '<div class="col p-0 justify-content-center align-self-center">'+
            //                         '<span class="m-0 month font-1p5rem"></span>'+                             
            //                     '</div>'+
            //                     '<div class="col _2rem p-0 justify-content-center align-self-center">'+
            //                         '<a class="btn btn-next px-0" >></a>'+                             
            //                     '</div>'+
            //                 '</div>'+
            //                 '</header>');
            var header = $('<header>' +
                '<div class="row m-0 ">' +
                '<div class="col p-0 justify-content-center align-self-center text-left">' +
                '<a class="btn  btn-calpriv px-0" ><</a>' +
                '</div>' +
                '<div class="col p-0 justify-content-center align-self-center">' +
                '<p class="m-0 month "></p>' +
                '</div>' +
                '<div class="col p-0 justify-content-center align-self-center text-right">' +
                '<a class="btn btn-calnext px-0" >></a>' +
                '</div>' +
                '</div>' +
                '</header>');
            this.updateHeader(todayDate, header);
            calendar.append(header);

            this.buildCalendar(todayDate, calendar);
            container.append(calendar);

            this.bindEvents();
        },

        //Update the current month header
        updateHeader: function(date, header) {
          
            header.find('.month').html(new Date(date).toISOString().substring(5, 7) + '/' + new Date(date).toISOString().substring(0, 4));
            header.find('.month').addClass('text calmonth');

        },

        //Build calendar of a month from date
        buildCalendar: function(fromDate, calendar) {
            var plugin = this;

            calendar.find('table').remove();

            var body = $('<table></table>');
            var thead = $('<thead></thead>');
            var tbody = $('<tbody></tbody>');

            //Header day in a week ( (1 to 8) % 7 to start the week by monday)

            for (var i = 0; i <= this.settings.days.length - 1; i++) {
                if (i == 0 || i == 6) {
                    thead.append($('<td class="weekend">' + this.settings.days[i % 7].substring(0, 3) + '</td>'));

                } else {
                    thead.append($('<td>' + this.settings.days[i % 7].substring(0, 3) + '</td>'));

                }

            }

            //setting current year and month
            var y = fromDate.getFullYear(),
                m = fromDate.getMonth(),
                d = fromDate.getDate();
           
            //first day of the month
            var firstDay = new Date(y, m, 1);
            
           
        
            //If not monday set to previous monday
            while (firstDay.getDay() != 0) {
                firstDay.setDate(firstDay.getDate() - 1);
            }

            //last day of the month
            var lastDay = new Date(y, m + 1, 0);
        
            //If not sunday set to next sunday
            while (lastDay.getDay() == 0) {
                lastDay.setDate(lastDay.getDate() + 1);
            }

            //For firstDay to lastDay
            for (var day = firstDay; day <= lastDay; day.setDate(day.getDate())) {
                var tr = $('<tr></tr>');
                //For each row

                for (var i = 0; i < 7; i++) {

              
                    if ((day.getMonth() + 1) < 10)
                        var date = day.getFullYear() + '-0' + (day.getMonth() + 1)
                    else
                        var date = day.getFullYear() + '-' + (day.getMonth() + 1)


                    if ((day.getDate()) < 10)
                        date += '-0' + day.getDate()
                    else
                        date += '-' + day.getDate()

                       

                    var td = $('<td class="day" onclick="selectDay(this)" data-date="' + date + '">' + day.getDate() + '</td>');
                    //if today is this day

                    if (i == 0 || i == 6) {
                        td.css("background", "#f9f9f9");
                    }
                    if (i == 6) {
                        td.css("background", "#f9f9f9");
                    }

                    if (day.toDateString() === (new Date).toDateString()) {
                        td.addClass("today");
                    }
                    let ymd = new Date().toISOString().substr(0, 10)
                    ymd = new Date(ymd)
                    ymd.setDate(ymd.getDate() - 1);

                    if (day < ymd) {
                        td.addClass("wrongday");
                    } else {

                    }

                    //if day is not in this month
                    if (day.getMonth() != fromDate.getMonth()) {
                        td.find(".day").addClass("wrong-month");
                        td.html('')
                        td.removeAttr('style');
                        td.attr('class', 'border-0');
                    }

                    tr.append(td);
                    day.setDate(day.getDate() + 1);
                }
                tbody.append(tr);
            }

            body.append(thead);
            body.append(tbody);

            var eventContainer = $('<div class="event-container"></div>');

            calendar.append(body);
            calendar.append(eventContainer);
        },
        //Init global events listeners
        bindEvents: function() {
            var plugin = this;

            //Click previous month
            $('.btn-calpriv').click(function() {
                plugin.currentDate.setMonth(plugin.currentDate.getMonth() - 1);
                plugin.buildCalendar(plugin.currentDate, $('.calendar'));
                plugin.updateHeader(plugin.currentDate, $('.calendar header'));
                updatecal($(this).index('.btn-calpriv'))
            });

            //Click next month
            $('.btn-calnext').click(function() {
                plugin.currentDate.setMonth(plugin.currentDate.getMonth() + 1);
                plugin.buildCalendar(plugin.currentDate, $('.calendar'));
                plugin.updateHeader(plugin.currentDate, $('.calendar header'));
                updatecal($(this).index('.btn-calnext'))
            });
        },
        formatToYYYYMMDD: function(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
    });

    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);
