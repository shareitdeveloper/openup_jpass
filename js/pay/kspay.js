function _pay(_frm) {
	// 1. 사전정보 set
	var agent 		= navigator.userAgent;
	var midx 		= agent.indexOf("MSIE");
	var out_size 	= (midx != -1 && agent.charAt(midx+5) < '7');
	
	var width_ 		= 500;
	var height_		= out_size ? 568 : 518;

	var left_		= screen.width;
	left_			= left_/2 - (width_/2);

	var top_		= screen.height;
	top_ 			= top_/2 - (height_/2);

	// 2.결제창 호출
	op = window.open('about:blank','AuthFrmUp','height='+height_+',width='+width_+',status=yes,scrollbars=no,resizable=no,left='+left_+',top='+top_+'');

	if (op == null) {
		alert("팝업이 차단되어 결제를 진행할 수 없습니다.");
		return false;
	}
	var timer = setInterval(function() { 
		if(op.closed) {
			clearInterval(timer);			  
			goResult(_frm,true);
		}
		}, 1000);

	// 3.결제정보 전송
	_frm.sndReply.value = "https://jpass.jeju.kr/pay/kspay/wh/rcv"; //결제정보를 수신할 페이지
	_frm.target = 'AuthFrmUp';
	
	_frm.action ='https://kspay.ksnet.to/store/KSPayFlashV1.3/KSPayPWeb.jsp?sndCharSet=utf-8';
	_frm.submit();

}

function goResult(_frm,cancel) {
	if(cancel){ 
		// 사용자 결제 취소 
		var input = document.createElement('input')
		input.setAttribute("type", "hidden");
		input.setAttribute("name","cancel");
		input.setAttribute("value", 1);
		document.KSPAY.appendChild(input)

		document.KSPAY.target = "";
		document.KSPAY.action = document.KSPAY.redirect_url.value; // 결제완료후 이동할 페이지
		document.KSPAY.method = "post";
		document.KSPAY.submit();	
	}else{
		// 사용자 결제 완료
		var input = document.createElement('input')
		input.setAttribute("type", "hidden");
		input.setAttribute("name","cancel");
		input.setAttribute("value", 0);
		document.KSPAY.appendChild(input)

		document.KSPAY.target = "";
		document.KSPAY.action = document.KSPAY.redirect_url.value; // 결제완료후 이동할 페이지
		document.KSPAY.method = "post";
		document.KSPAY.submit();
	}
}

// eparamSet() - 함수설명 : 결재완료후 (kspay_wh_rcv.php로부터)결과값을 받아 지정된 결과페이지(kspay_wh_result.php)로 전송될 form에 세팅합니다.
function eparamSet(rcid, rctype, rhash) {
	document.KSPAY.reWHCid.value 	= rcid;
	document.KSPAY.reWHCtype.value   = rctype;
	document.KSPAY.reWHHash.value 	= rhash;		
}