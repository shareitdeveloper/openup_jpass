// 1. BASE64 인코딩 & 디코딩 함수 (한글을 그냥 날리면 깨짐)
function Base64Encode(str, encoding = 'utf-8') {
    var bytes = new (TextEncoder || TextEncoderLite)(encoding).encode(str);        
    return base64js.fromByteArray(bytes);
}

function Base64Decode(str, encoding = 'utf-8') {
    var bytes = base64js.toByteArray(str);
    return new (TextDecoder || TextDecoderLite)(encoding).decode(bytes);
}

// 2. 모바일 판단 함수
var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false; 

// 3. 결제 정보 초기화
function payinit(params){
console.log(params)
    if(params.pg == 'kspay'){  
        //3. 스크립트 동적 삽입 (PG에 따른 스크립트가 다르기 때문)
        var head    = document.getElementsByTagName('head')[0];
        var script  = document.createElement('script');
        script.type = 'text/javascript';
        
        if(isMobile) {
            //3-1. 모바일용
            script.src = '/js/pay/kspaymobile.js';
        }else{
            //3-2. PC용
            script.src = '/js/pay/kspay.js';
        }
        
        head.appendChild(script)
        
    }else if(params.pg == "nicepay"){
        var head    = document.getElementsByTagName('head')[0];
        var script  = document.createElement('script');
        script.type = 'text/javascript';
        if(isMobile) {
            // 모바일용
            script.src = 'https://web.nicepay.co.kr/v3/v3Payment.jsp';
        }else{
            // PC용
            script.src = 'https://web.nicepay.co.kr/v3/webstd/js/nicepay-3.0.js';
        }
        head.appendChild(script)
        var script2  = document.createElement('script');
        script2.type = 'text/javascript';
        script2.src = '/js/pay/nicepay.js';
        head.appendChild(script2)
        
    }
}


// 4. 결제창 호출
function pgpay(params){

    if(params.pg == 'kspay') { // KSPAY 사 일 때
        var body = document.getElementsByTagName('body')[0]
        var form = document.createElement('form')
        var txt = ''

        // 5. 안해주면 결제창에서 한글 깨짐
        form.setAttribute("accept-charset", "EUC-KR");       
        console.log(params)
        // 6. 결제창 호출 파타미터의 내용중 한글이 포함된 내용 으로 BASE64 인코딩 진행
        for ( var key in params) {
            if(key == 'ItemName' || key == 'MemberName' || key == 'ResvName' || key == 'Memo'){
                txt += '{'+key+'}{:}'+ Base64Encode(params[key]) + '|'
            }else{
                txt += '{'+key+'}{:}'+ params[key] + '|'
            }
        }


        var Goodname = params.ItemName
        var ItemCount = params.checkedItemCount - 1
        if (params.Page == 'Cart' && ItemCount > 0) {
            Goodname = params.ItemName + ' 외 ' + ItemCount + ' 개'
        }

        // 7. 결제창 호출 파라미터 SET
        var senddata = {
            'sndStoreid':'2999199999', // 일단 TESTID로 고정
            'sndOrdernumber':params.merchant_uid,
            'sndPaymethod':'1000000000', // 일단 CARD 로 고정
            'sndGoodname':Goodname,
            'sndAmount':params.TotalAmount,            
            'sndOrdername':params.MemberName,
            'sndEmail':params.Email,
            'sndMobile':params.Phone.replace(/-/gi, "") ,
            'sndServicePeriod':'',
            'sndReply':'',
            'sndGoodType':1,
            'sndShowcard':'I,M',
            'sndCurrencytype':'WON',
            'sndInstallmenttype':'ALL(0:2:3:4:5:6:7:8:9:10:11:12)',
            'sndInteresttype':'NONE',
            'sndEscrow':1,
            'sndCashReceipt':0,
            'reWHCid':'',
            'reWHCtype':'',
            'reWHHash':'',
            'a':'',
            'b':'',
            'c':'',
            'd':'',
            'ECHA':txt,
            'ECHB':'M2',
            'ECHC':'M3',
            'redirect_url':params.redirect_url,
            'TotalAmount':params.TotalAmount,
            'amt':params.qty,
            'MemberName':params.MemberName,
            'Email':params.Email,
            'Phone':params.Phone,
            'supid':params.supid,
            'affid' :params.affid,        
            'memberid':params.memberid,
            'image':params.image,
            'ResvDate':params.ResvDate,
            'ResvName':params.ResvName,
            'ResvTel':params.ResvTel,
            'Memo':params.Memo,         
            'ItemResvOpt_Seq':params.ItemResvOpt_Seq
        }

        // 8. JSON to FORM (FROM 동적 생성)
        for ( var key in senddata ) {
            var input = document.createElement('input')
            input.setAttribute("type", "hidden");
            input.setAttribute("name",key);
            input.setAttribute("value", senddata[key]);
            form.appendChild(input)
        }
    
        form.setAttribute("name","KSPAY");

        body.appendChild(form)

        // 9. 결제창 호출
       _pay(document.KSPAY);


    } else if(params.pg == 'nicepay') { // NICEPAY 사 일 때

        console.log("nicepay params: ", params)

        var body = document.getElementsByTagName('body')[0]
        var form = document.createElement('form')
        var txt = ''

        var Goodname = params.ItemName
        var ItemCount = params.checkedItemCount - 1
        if (params.Page == 'Cart' && ItemCount > 0) {
            Goodname = params.ItemName + ' 외 ' + ItemCount + ' 개'
        }

        if(isMobile) {
            // 모바일용
            var senddata = {
                'PayMethod':'CARD',
                'GoodsName':Goodname,
                'Amt':params.TotalAmount,
                'MID':params.storeid,
                'Moid':params.DealTRCode,
                'BuyerName':params.MemberName,
                'BuyerEmail':params.Email,
                'BuyerTel':params.Phone,
                'ReturnURL':params.redirect_url,
                'VbankExpDate':'',
                'NpLang':'KO',
                'GoodsCl':'1',
                'TransType':'0',
                'CharSet':'utf-8',
                'ReqReserved':'nicepay'+'[|]'+params.image+'[|]'+params.MemberName+'[|]'+params.Email+'[|]'+params.Phone,
                'image':params.image,
                'EdiDate':params.ediDate,
                'SignData':params.signdate,
                'Page':params.Page
            }
        }else{
            // PC용
            var senddata = {
                'PayMethod':'CARD',
                'GoodsName':Goodname,
                'Amt':params.TotalAmount,
                'MID':params.storeid,
                'Moid':params.DealTRCode,
                'BuyerName':params.MemberName,
                'BuyerEmail':params.Email,
                'BuyerTel':params.Phone,
                'VbankExpDate':'',
                'NpLang':'KO',
                'GoodsCl':'1',
                'TransType':'0',
                'CharSet':'utf-8',
                'ReqReserved':'nicepay'+'[|]'+params.image+'[|]'+params.MemberName+'[|]'+params.Email+'[|]'+params.Phone,
                'image':params.image,
                'EdiDate':params.ediDate,
                'SignData':params.signdate,
                'Page':params.Page
            }
        }

        // 7. 결제창 호출 파라미터 SET
        // var senddata = {
        //     'PayMethod':'CARD',
        //     'GoodsName':Goodname,
        //     'Amt':params.TotalAmount,
        //     'MID':params.storeid,
        //     'Moid':params.DealTRCode,
        //     'BuyerName':params.MemberName,
        //     'BuyerEmail':params.Email,
        //     'BuyerTel':params.Phone,
        //     'ReturnURL':params.redirect_url,
        //     'VbankExpDate':'',
        //     'NpLang':'KO',
        //     'GoodsCl':'1',
        //     'TransType':'0',
        //     'CharSet':'utf-8',
        //     'ReqReserved':'nicepay'+'[|]'+params.image+'[|]'+params.MemberName+'[|]'+params.Email+'[|]'+params.Phone,
        //     'image':params.image,
        //     'EdiDate':params.ediDate,
        //     'SignData':params.signdate,
        //     'Page':params.Page
        // }

        // 8. JSON to FORM (FROM 동적 생성)
        for ( var key in senddata ) {
            var input = document.createElement('input')
            input.setAttribute("type", "hidden");
            input.setAttribute("name",key);
            input.setAttribute("value", senddata[key]);
            form.appendChild(input)
        }

        form.setAttribute("method","post");

        form.setAttribute("action","/member/payresult");

        form.setAttribute("name","payForm");

        body.appendChild(form)

        // 9. 결제창 호출
        _pay(document.payForm);
    }
}