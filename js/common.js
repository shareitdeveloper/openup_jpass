$(window).hide();

// window.onpageshow = function (event) {

//     if (event.persisted || (window.performance && window.performance.navigation.type == 2)) {

//      //   location.reload();

//     }

// };

function isEmoji(str) {
    var ranges = [
        '[\uE000-\uF8FF]',
        '\uD83C[\uDC00-\uDFFF]',
        '\uD83D[\uDC00-\uDFFF]',
        '[\u2011-\u26FF]',
        '\uD83E[\uDD10-\uDDFF]'
    ];
    if (str.match(ranges.join('|'))) {
        return true;
    } else {
        return false;
    }
}

function hideVirtualKeyboard() {
    if (document.activeElement && document.activeElement.blur && typeof document.activeElement.blur === 'function') {
        document.activeElement.blur()
    }
}

function convert12H(a) {
    var time = a;
    var getTime = time.substring(0, 2);
    var intTime = parseInt(getTime);

    if (intTime < 12) {
        var str = '오전 ';
    } else {
        var str = '오후 ';
    }

    if (intTime == 12) {
        var cvHour = intTime;
    }
    else {
        var cvHour = intTime % 12;
    }

    var res = str + ('0' + cvHour).slice(-2) + time.slice(-3);

    return res;
}


function CALL_API(data, success, fail, err) {

    $.post("/api/execute", data, function (jqXHR) {

        if (jqXHR.msg == 'SUCCESS') {
            success(jqXHR);
            return;
        } else {
            fail(jqXHR);
            return;
        }
    }, 'json')
        .done(function (jqXHR) { if (jqXHR.msg != 'SUCCESS') { err(jqXHR) } })
        .fail(function (jqXHR) { if (jqXHR.msg != 'SUCCESS') { err(jqXHR) } })
        .always(function (jqXHR) { if (jqXHR.msg != 'SUCCESS') { err(jqXHR) } })
}


// setTimeout(function () {
//     pcresize();
// }, 200);

function addBtnClick () {
    addBtn.click();

    $("#add_shortcut").click(function(){
        homeicon_install();
    });
}

function setRedirect(str, num) {
    document.cookie = 'redirecturl_' + num + '=' + str + ';Path=/';

}

function changeTitle (left, center, right) {
    const topleft = document.querySelectorAll(".col.col-20.m-0.p-0.text-left");
    const title = document.querySelector(".col.m-0.p-0.title");
    const topright = document.querySelectorAll(".col.col-20.m-0.p-0.text-right");
    topleft.innerHTML = left;
    title.innerHTML = center;
    topright.innerHTML = right;
};

function move (txt, replace, newWin) {

    if (txt == 'getRedirectURL') {

        if (document.referrer) {
            history.back();
        } else {
            location.href = '/';
        }

    } else if ( txt=='/user/main') {
        // location.replace(txt)

        // setskin (도메인사이트)) 메인못가게 통제.
        if (window.location.hostname === 'visitgotjawal.kr' ) {
            location.replace('/affiliate/showstore/326') // .
        } else if (window.location.hostname === 'store.jejunia.com' ) {
            location.replace('/affiliate/showstore/3') // .)
           
        } else {
            location.replace(txt)
        }

    } else {
        if (replace && replace != '') {
            location.replace(txt)
        } else {
            if (newWin == 'true') {
                var newWindow = window.open("about:blank");
                newWindow.location.href = txt;
            } else {
                location.href = txt;
            }
        }
    }

    //location.replace(txt)
};

function closeWindow () {
    window.open('','_self').close();
}

function pad(n, width) {
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}


function getRedirectURL(num) {
    //history.back();
    return 'getRedirectURL';
    // var value = document.cookie.match('(^|;) ?' + 'redirecturl_' + num + '=([^;]*)(;|$)');
    // return value ? value[2] : null;
    // var x, y;
    // var val = document.cookie.split(';');

    // for (var i = 0; i < val.length; i++) {
    //     x = val[i].substr(0, val[i].indexOf('='));
    //     y = val[i].substr(val[i].indexOf('=') + 1);
    //     x = x.replace(/^\s+|\s+$/g, ''); // 앞과 뒤의 공백 제거하기

    //     if (x == 'redirecturl_'+num) {
    //         if (unescape(y))
    //             return unescape(y); // unescape로 디코딩 후 값 리턴
    //         else
    //             return '/'
    //     }
    // }
}

function changeAmount(classname, value, idx) {
    // classname 는 단일 클래스임 클래스가 중복 되서는 안됨
    // onclick="changeAmount('dayamount',1)"
    //  onclick="changeAmount('itemamount',-1)" 등등
    //  value 는 증감 숫자
    let num = null;

    if (idx) {
        num = idx;
    } else {
        num = 0;
    }

    const obj = document.getElementsByClassName(classname)[num];

    if (parseInt(obj.value) + parseInt(value) >= 0) {
        obj.value = parseInt(obj.value) + parseInt(value);
    }

    if (parseInt(obj.value) == 0) {
        obj.value = 1
    }

};

function pcresize() {

    var filter = "win16|win32|win64|mac|macintel";
    if (navigator.platform) {
        if (filter.indexOf(navigator.platform.toLowerCase()) < 0) {

        } else {
            $('body').css('width', '410px')
            $('#findaddr').css('width', '410px')
            $('#findaddr').css('margin', '0 auto')
            $('#findaddr').css('left', 'unset')
            $('body').css('height', 'auto')

            // console.log('pcresize: height',$(document).height())

            if ($(window).height() == $(document).height()) {
                $('body').css('height', $(document).height())

            } else {
                $('body').css('height', 'auto')

            }

            var size = {
                width: window.innerWidth || document.body.clientWidth,
                height: window.innerHeight || document.body.clientHeight
            }

            $('body').css('margin', '0 auto')
            $('body').css('border', '1px solid #dbdbdb')

            $('.main-store-image').css('height', '290px')
            $('.main-store-image').css('width', '385px')
            $('.main-store-image').css('overflow', 'hidden')

            $('.profile .left').css('left', (size.width/2)-205 + 'px' )
            $('.profile .right').css('right', (size.width/2)-205 + 'px' )


            $('.bx-wrapper').css('max-width', '410px')
            $('.bx-wrapper').css('width', '410px')

            $('.bxslider > div').css('width', '100%')
            $('.fixed-bottom').css('width', '410px')
            $('.fixed-bottom').css('margin', '0 auto')
            $('.fixed-bottom').css('max-width', '410px')
            $('#event-cal-container').css('width', '100%')
            $('#event-cal-container').css('margin', '0 auto')
            $('#event-cal-container').css('position', 'unset')
            $('#event-cal-container').css('background', 'unset')
            $('.icon-imgdel').css('background-size', '50%')
            $('.icon-imgdel').css('width', '3vw')
            $('.icon-expose').css('width', '35%')
            $('.fixed-bottom').removeClass('m-0')
            $('.fixed-bottom').removeClass('mt-3')
            $('.fixed-bottom').css('border', '1px solid #dbdbdb')
            $('.fixed-bottom').css('border-width', '0px 1px 1px 1px')
            // $('.count').css('position', 'relative')
            // $('.count').css('top', '-20px')
            // $('.count').css('left', '360px')
            // $('.count').css('z-index', '9999')
            $('#findaddr').css('width', '408px');
            $('#findaddr').css('margin', '0 auto');
        }
    }

}


function insertComma(obj) {
    // obj 는 text 입력 때만 가능함
    //onkeyup="insertComma(this)"
    // accounting.js 라이브러리를 사용함
    //  링크 : http://openexchangerates.github.io/accounting.js/
    obj.value = accounting.formatMoney(obj.value, "", 0, ",", ".");

};

function countWord (obj, dest, max, idx) {
    // obj 는 textbox 임
    // dest 는 남은 숫자 영역을 클래스 이름으로 가져오고 중복 되서는 안됨
    //  onkeyup="countWord(this, 'remainitemtitle', 64)"
    //  max는 최대 입력 수 이고 자동으로 최대 입력수에 맞게 잘림
    let remain = null;
    if (idx)
        remain = document.getElementsByClassName(dest)[idx];
    else
        remain = document.getElementsByClassName(dest)[0];

    if (obj.value.length > max) {
        obj.value = new String(obj.value).slice(0, max - obj.value.length);
    }
    remain.innerHTML = obj.value.length + '/' + max;
};

function SHA256(s){

    var chrsz   = 8;
    var hexcase = 0;

    function safe_add (x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
    function R (X, n) { return ( X >>> n ); }
    function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
    function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
    function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
    function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
    function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
    function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }

    function core_sha256 (m, l) {

        var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1,
            0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
            0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786,
            0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
            0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147,
            0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
            0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B,
            0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
            0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A,
            0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
            0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);

        var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);

        var W = new Array(64);
        var a, b, c, d, e, f, g, h, i, j;
        var T1, T2;

        m[l >> 5] |= 0x80 << (24 - l % 32);
        m[((l + 64 >> 9) << 4) + 15] = l;

        for ( var i = 0; i<m.length; i+=16 ) {
            a = HASH[0];
            b = HASH[1];
            c = HASH[2];
            d = HASH[3];
            e = HASH[4];
            f = HASH[5];
            g = HASH[6];
            h = HASH[7];

            for ( var j = 0; j<64; j++) {
                if (j < 16) W[j] = m[j + i];
                else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);

                T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                T2 = safe_add(Sigma0256(a), Maj(a, b, c));

                h = g;
                g = f;
                f = e;
                e = safe_add(d, T1);
                d = c;
                c = b;
                b = a;
                a = safe_add(T1, T2);
            }

            HASH[0] = safe_add(a, HASH[0]);
            HASH[1] = safe_add(b, HASH[1]);
            HASH[2] = safe_add(c, HASH[2]);
            HASH[3] = safe_add(d, HASH[3]);
            HASH[4] = safe_add(e, HASH[4]);
            HASH[5] = safe_add(f, HASH[5]);
            HASH[6] = safe_add(g, HASH[6]);
            HASH[7] = safe_add(h, HASH[7]);
        }
        return HASH;
    }

    function str2binb (str) {
        var bin = Array();
        var mask = (1 << chrsz) - 1;
        for(var i = 0; i < str.length * chrsz; i += chrsz) {
            bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
        }
        return bin;
    }

    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    }

    function binb2hex (binarray) {
        var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var str = "";
        for(var i = 0; i < binarray.length * 4; i++) {
            str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
                hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
        }
        return str;
    }

    s = Utf8Encode(s);
    return binb2hex(core_sha256(str2binb(s), s.length * chrsz));

}


function txtCheck(type, inputobj, checkstr, length) {

    if(!checkstr) checkstr = ''
    if(!length) length = 0
    // // ALL - 전체문자
    // // N - 숫자만
    // // S - 특수기호
    // // E - 영어만
    var regEx = '';
    var checkregex = 'regex';
    var value = $(inputobj).val();
    // console.log(value)
    var failmsg = '';

    if (type == "PHONE") {
        failmsg = "전화번호 형식에 맞지 않습니다."
        regEx = /(((^02|^031|^032|^033|^041|^042|^043|^051|^052|^053|^054|^055|^061|^062|^063|^064|^070|^010|^011|^016|^017|^018|^019|^050|^070|^080)-([0-9]{3,4})-([0-9]{4}))|(^15[0-9]{2}|^16[0-9]{2}|^18[0-9]{2})-([0-9]{4}))$/
        // regEx = new RegExp('^0([2|31|32|33|41|42|43|51|52|53|54|55|61|62|63|64|70]?)-?([0-9]{4})-?([0-9]{4})$')
    } else if (type == "CELLPHONE") {
        failmsg = "휴대전화번호 형식에 맞지 않습니다."
        //하이픈(-) 없이 숫자만 입력받기
        regEx = new RegExp('^01([0|1|6|7|8|9]{1})([0-9]{3,4})([0-9]{4})$')
    } else if (type == "EMAIL") {
        failmsg = "이메일 형식에 맞지 않습니다."
        regEx = new RegExp('^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*[.][a-zA-Z]{2,3}$', 'i')
    } else if (type == "PASS") {
        failmsg = "패스워드 형식에 맞지 않습니다."
        regEx = /(?=.*\d)(?=.*[a-z]).{8,15}/i
    } else if (type == "NAME") {
        failmsg = "올바른 이름이 아닙니다."
        regEx = /^[a-z가-힣]+$/
    } else if (type == "PARTNERSUGGEST") {
        failmsg = "신청메시지를입력하세요"
        checkregex = 'space'
    } else if (type == "REVDATE") {
        failmsg = "예약일을 선택하세요"
        checkregex = 'space'
    } else if (type == 'REVAMT') {
        failmsg = "예약 수량을 선택하세요"
        checkregex = 'zero'
    } else if (type == 'SHIPPINGDATE') {
        failmsg = "배송 예약일을 선택하세요"
        checkregex = 'space'
    } else if (type == 'HOTELAMT') {
        failmsg = "투숙 인원을 선택하세요"
        checkregex = 'zero'
    } else if (type == 'HOTELDATE') {
        failmsg = "투숙 인원을 선택하세요"
        checkregex = 'space'
    } else if (type == 'ITEMAMT') {
        failmsg = "상품 수량을 선택하세요"
        checkregex = 'zero'
    } else if (type == 'BANKACCOUNT') {
        regEx = /^[0-9*#+_-]{1,24}$/
    }

    if (checkregex == 'regex') {
        if (value.match(regEx) != null) {
            return true;
        } else {
            modal(failmsg, 'ok', 'hide', 'yes', 'no')
            return false;
        }
    } else if (checkregex == 'space') {
        if (value) {
            return true;
        } else {
            modal(failmsg, 'ok', 'hide', 'yes', 'no')
            return false;
        }
    } else if (checkregex == 'zero') {
        if (value != 0) {
            return true;
        } else {
            modal(failmsg, 'ok', 'hide', 'yes', 'no')
            return false;
        }

    }

}

/**
 * input tag value check when out of focus
 *
 * @param obj input object
 */
let inputFocusOut = function(obj){
    let inputValue = parseInt(obj.value)
    if ( inputValue < 1 ) {
        obj.value = 1;
    } else if ( inputValue > 99999 ) {
        obj.value = 99999;
    }
}

/**
 * 소셜로그인 분기 모달
 *
 * @param platform Naver,Kakao,Google
 */
function socialLoginModal(platform) {
    if( platform === 'NAV' ) {    //네이버

    } else if ( platform === 'KAO' ) {    //카카오
        $('.modal.social').modal({
            backdrop: 'static',
            keyboard: false
        });
    } else if ( platform === 'GOO' ) {   //구글

    } else {
        $('.modal').modal('hide')
    }
}

function modal(msg, param, okFun, yesFun, noFun, yesText, noText) {

    /*
    파라미터 설명
    msg: 모달창에 노출되는 텍스트
    param: 모달창 버튼 종류 지정 (ok, yesno)
    okFun: param이 ok 일 때 동작할 행동 (함수)
    yesFun: param이 yesno 일 때, 동작할 행동 (함수)
    noFun: param이 yesno 일 때, 동작할 행동 (함수)
    yesText: 인자가 있을 경우, yes 버튼 텍스트를 해당 인자로 변경
    noText: 인자가 있을 경우, no 버튼 텍스트를 해당 인자로 변경
    */

    if (param == "ok") {
        $('.modal.ok .modal-body').html(modalmsg(msg));
        // $(element).modal('show')
        $('.modal.ok').modal({
            backdrop: 'static',
            keyboard: false
        });

        // $('.modal.ok .modal-footer>button:eq(0)').html(modalmsg('confirmbtn'));
        if (okFun == 'hide') {
            $('.modal.ok .modal-footer>button:eq(0)').attr('onclick', '$(\'.modal.ok\').modal(\'hide\')');
        } else {
            $('.modal.ok .modal-footer>button:eq(0)').attr('onclick', okFun);
        }

        if (yesText) {
            $('.modal.ok .modal-footer>button:eq(0)').html(yesText);
        }

    } else if (param == "yesno") {
        $('.modal.yesno .modal-body').html(modalmsg(msg));
        // $(element).modal('show')
        $('.modal.yesno').modal({
            backdrop: 'static',
            keyboard: false
        });

        // $('.modal.yesno .modal-footer>button:eq(0)').html(modalmsg('nobtn'));
        // $('.modal.yesno .modal-footer>button:eq(1)').html(modalmsg('yesbtn'));

        if (noFun == 'hide') {
            $('.modal.yesno .modal-footer>button:eq(0)').attr('onclick', '$(\'.modal.yesno\').modal(\'hide\')');
        } else {
            $('.modal.yesno .modal-footer>button:eq(0)').attr('onclick', noFun);
        }

        if (yesFun == 'hide') {
            $('.modal.yesno .modal-footer>button:eq(1)').attr('onclick', '$(\'.modal.yesno\').modal(\'hide\')');
        } else {
            $('.modal.yesno .modal-footer>button:eq(1)').attr('onclick', yesFun);
        }

        if (yesText) {
            $('.modal.yesno .modal-footer>button:eq(1)').html(yesText);
        }
        if (noText) {
            $('.modal.yesno .modal-footer>button:eq(0)').html(noText);
        }

    } else if (param === 'share') {
        $('.modal.share').modal({
            backdrop: 'static',
            keyboard: false
        });

        let msgObj = msg.split('|')
        console.log(msgObj)

        $('.modal.share .modal-body #share-Copylink').attr('onclick','copyToClipboard(\''+ msgObj[2] +'\')');
        $('.modal.share .modal-body #share-Kakaotalk').attr('onclick','shareToKakao(\''+ msgObj[0] +'\',\''+ msgObj[1] +'\',\'' + msgObj[2] + '\',\'' + msgObj[3] + '\')');
        $('.modal.share .modal-body #share-Faceboook').attr('onclick','shareTo(\''+ msgObj[0] +'\',\''+ msgObj[1] +'\',\'' + msgObj[2] + '\',\'facebook\')');
        $('.modal.share .modal-body #share-Twitter').attr('onclick','shareTo(\''+ msgObj[0] +'\',\''+ msgObj[1] +'\',\'' + msgObj[2] + '\',\'twitter\')');
        //닫기버튼 옵션
        $('.modal.share .modal-footer>button:eq(0)').attr('onclick', '$(\'.modal.share\').modal(\'hide\')');
    } else if (param === 'excelDownload') {
        $('.modal.selectdelv_download').modal({
            backdrop: 'static',
            keyboard: false
        });


        //닫기버튼 옵션
        $('.modal.selectdelv_download .modal-footer>button:eq(0)').attr('onclick', '$(\'.modal\').modal(\'hide\')');
    }
};


function setCookie (name, value, exp) {
    if(!exp) exp  = 1
    var date = new Date();
    date.setTime(date.getTime() + exp *24*60*60*1000);
    /**
     * 2021.07.09 고금필
     *
     * escape() 쿠키 저장시 한글 깨짐 방지
     * 해당조치를 하지않을경우 사파리에서 깨진쿠키를 전부 제거해버려서 일부기능이 작동 하지않음
     */
    document.cookie = name + '=' + escape(value) + ';expires=' + date.toUTCString() + ';path=/';
};

function getCookie (name) {
    var value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    /**
     * 2021.07.09 고금필
     *
     * unescape() 쿠키 저장시 한글 깨짐 방지 decode
     */
    return value ? unescape(value[2]) : null;
};

// 쿠키삭제 함수 추가
function delCookie (name) {
    let date = new Date();
    date.setDate(date.getDate() - 100);
    let Cookie = `${name}=;Expires=${date.toUTCString()}`
    document.cookie = Cookie;
}

function indexInParent(node) {
    const children = node.parentNode.childNodes;
    let num = 0;
    for (var i = 0; i < children.length; i++) {
        if (children[i] == node) return num;
        if (children[i].nodeType == 1) num++;
    }
    return -1;
};

function  getDateRange(startDate, endDate, listDate) {
    const dateMove = new Date(startDate);
    strDate = startDate;
    if (startDate == endDate) {
        const strDate = dateMove.toISOString().slice(0, 10);
        listDate.push(strDate);
    } else {
        while (strDate < endDate) {
            var strDate = dateMove.toISOString().slice(0, 10);
            listDate.push(strDate);
            dateMove.setDate(dateMove.getDate() + 1);
        }
    }
    return listDate;
};

function selectOne (txt, obj, mode) {
    let name = document.getElementsByName(txt);
    let i;

    for (i = 0; i < name.length; i++) {
        name[i].classList.remove(mode);
        name[i].classList.remove('selected');
    }

    obj.classList.add(mode);
    obj.classList.add('selected');
}

function selectOne_active(txt, obj, mode) {
    let name = document.getElementsByName(txt);
    let i;

    for (i = 0; i < name.length; i++) {

        name[i].classList.remove('active');
    }


    obj.classList.add('active');
}



function activeOne(num,className) { // className=>@optional ( use in '~/ruturn' )
    if(className){
        let classNameEle = document.getElementsByClassName(className)  
        var i;
        for (i = 0; i < classNameEle.length; i++) {
            classNameEle[i].classList.remove('active');
        }
        classNameEle[num].classList.add("active");
    }

    if(!className) {
   
        var checkcircle = document.getElementsByClassName("checkcircle");
        var i;
        for (i = 0; i < checkcircle.length; i++) {
            checkcircle[i].classList.remove('active');
        }
        checkcircle[num].classList.add("active");
    }
}

// Sns select (dropdown)
function getSnsChannelHtml(type, count) {
    // type:  select ||
    let returnHtml = ''
    if (type == 'select') {
        returnHtml = '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 0)"><img src="/img/icon/sns/etc-64.png" width="16px" alt="기타 아이콘"/><span class="pl-2">기타</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 1)"><img src="/img/icon/sns/nblog-64.png" width="16px" alt="네이버 블로그 아이콘"/><span class="pl-2">네이버 블로그</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 2)"><img src="/img/icon/sns/insta-64.png" width="16px" alt="인스타그램 아이콘"/><span class="pl-2">인스타그램</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 3)"><img src="/img/icon/sns/youtube-64.png" width="16px" alt="유튜브 아이콘"/><span class="pl-2">유튜브</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 4)"><img src="/img/icon/sns/facebook-64.png" width="16px" alt="페이스북 아이콘"/><span class="pl-2">페이스북</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 5)"><img src="/img/icon/sns/twitter-64.png" width="16px" alt="트위터 아이콘"/><span class="pl-2">트위터</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 6)"><img src="/img/icon/sns/band-64.png" width="16px" alt="밴드 아이콘"/><span class="pl-2">네이버 밴드</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 7)"><img src="/img/icon/sns/ncafe-64.png" width="16px" alt="네이버 카페 아이콘"/><span class="pl-2">네이버 카페</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 8)"><img src="/img/icon/sns/dcafe-64.png" width="16px" alt="다음 카페 아이콘"/><span class="pl-2">다음 카페</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 9)"><img src="/img/icon/sns/kakaostory-64.png" width="16px" alt="카카오 스토리 아이콘"/><span class="pl-2">카카오 스토리</span></a><div class="dropdown-divider"></div>'

        returnHtml = returnHtml + '<a class="dropdown-item  p-0 px-3" onclick="setSnsChannel(' + count + ', 10)"><img src="/img/icon/sns/tstory-64.png" width="16px" alt="티스토리 아이콘"/><span class="pl-2">티스토리</span></a>' //<div class="dropdown-divider"></div> 끝줄제거
    }
    return returnHtml
}

// Sns click 및 return
function setSnsChannel(idx, num, ReturnImg,width) {
    let snsImg = ''
    let imgWidth = 16
    if(width > 0 ) {
        imgWidth = width
    }
    if(width > 64) { // png 사이즈가 64이므로 제한
        imgWidth = 64
    }

    if (num == 0) {
        snsImg = '<img src="/img/icon/sns/etc-64.png" width="'+imgWidth+'px" alt="기타 아이콘"/><span class="pl-2">기타</span>'

    } else if (num == 1) {
        snsImg = '<img src="/img/icon/sns/nblog-64.png" width="'+imgWidth+'px" alt="네이버 블로그 아이콘"/><span class="pl-2">네이버 블로그</span>'

    } else if (num == 2) {
        snsImg = '<img src="/img/icon/sns/insta-64.png" width="'+imgWidth+'px" alt="인스타그램 아이콘"/><span class="pl-2">인스타그램</span>'

    } else if (num == 3) {
        snsImg = '<img src="/img/icon/sns/youtube-64.png" width="'+imgWidth+'px" alt="유튜브 아이콘"/><span class="pl-2">유튜브</span>';

    } else if (num == 4) {
        snsImg = '<img src="/img/icon/sns/facebook-64.png" width="'+imgWidth+'px" alt="페이스북 아이콘"/><span class="pl-2">페이스북</span>'

    } else if (num == 5) {
        snsImg = '<img src="/img/icon/sns/twitter-64.png" width="'+imgWidth+'px" alt="트위터 아이콘"/><span class="pl-2">트위터</span>'

    } else if (num == 6) {
        snsImg = '<img src="/img/icon/sns/band-64.png" width="'+imgWidth+'px" alt="밴드 아이콘"/><span class="pl-2">네이버 밴드</span>'

    } else if (num == 7) {
        snsImg = '<img src="/img/icon/sns/ncafe-64.png" width="'+imgWidth+'px" alt="네이버 카페 아이콘"/><span class="pl-2">네이버 카페</span>'

    } else if (num == 8) {
        snsImg = '<img src="/img/icon/sns/dcafe-64.png" width="'+imgWidth+'px" alt="다음 카페 아이콘"/><span class="pl-2">다음 카페</span>'

    } else if (num == 9) {
        snsImg = '<img src="/img/icon/sns/kakaostory-64.png" width="'+imgWidth+'px" alt="카카오 스토리 아이콘"/><span class="pl-2">카카오 스토리</span>'

    } else if (num == 10) {
        snsImg = '<img src="/img/icon/sns/tstory-64.png" width="'+imgWidth+'px" alt="티스토리 아이콘"/><span class="pl-2">티스토리</span>'

    }

    // 분기처리
    if(ReturnImg && ReturnImg=='Y') { // snsImg return
        return snsImg
    } else {
        $('.snsIdx'+idx).html(snsImg);
    }
}

// 기존 icon 형식 함수
// function channel(idx, num) {
//     var selectedSns = document.querySelectorAll(".selectedSns");
//     var chAddr = document.querySelectorAll(".chAddr");

//     chAddr[idx].focus();
//     if (num == 0) {
//         selectedSns[idx].innerHTML = "직접입력";
//     } else if (num == 1) {
//         selectedSns[idx].innerHTML = "<i class='icon-nblog' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>네이버 블로그</span>";
//         //  chAddr[idx].value = "blog.naver.com/";

//     } else if (num == 2) {
//         selectedSns[idx].innerHTML = "<i class='icon-band' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>네이버 밴드</span>";
//         //   chAddr[idx].value = "band.us/";

//     } else if (num == 3) {
//         selectedSns[idx].innerHTML = "<i class='icon-branch' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>브런치</span>";
//         //  chAddr[idx].value = "brunch.co.kr/@";

//     } else if (num == 4) {
//         selectedSns[idx].innerHTML = "<i class='icon-youtube' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>유튜브</span>";
//         //  chAddr[idx].value = "youtu.be/";

//     } else if (num == 5) {
//         selectedSns[idx].innerHTML = "<i class='icon-insta' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>인스타그램</span>";
//         //   chAddr[idx].value = "www.instagram.com/";

//     } else if (num == 6) {
//         selectedSns[idx].innerHTML = "<i class='icon-kakaostory' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>카카오 스토리</span>";
//         //  chAddr[idx].value = "story.kakao.com/";

//     } else if (num == 7) {
//         selectedSns[idx].innerHTML = "<i class='icon-twitter' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>트위터</span>";
//         //  chAddr[idx].value = "www.twitter.com/";

//     } else if (num == 8) {
//         selectedSns[idx].innerHTML = "<i class='icon-tstory' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>티스토리</span>";
//         // chAddr[idx].value = ".tistory.com";

//     } else if (num == 9) {
//         selectedSns[idx].innerHTML = "<i class='icon-facebook' style='position:absolute;margin-top:-2px;'></i><span class='pl-4'>페이스북</span>";
//         // chAddr[idx].value = "www.facebook.com/";
//     }
// }



function getUrlParams() {
    var params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (str, key, value) { params[key] = value; });
    return params;
}

String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); };

String.prototype.ltrim = function () { return this.replace(/^\s+/, ''); };

String.prototype.rtrim = function () { return this.replace(/\s+$/, ''); };

String.prototype.fulltrim = function () { return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' '); };

Date.prototype.format = function (t) {
    if (!this.valueOf()) return " ";
    var e = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
        r = this;
    return t.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function (t) {
        switch (t) {
            case "yyyy":
                return r.getFullYear();
            case "yy":
                return (r.getFullYear() % 1e3).zf(2);
            case "MM":
                return (r.getMonth() + 1).zf(2);
            case "dd":
                return r.getDate().zf(2);
            case "E":
                return e[r.getDay()];
            case "HH":
                return r.getHours().zf(2);
            case "hh":
                return ((h = r.getHours() % 12) ? h : 12).zf(2);
            case "mm":
                return r.getMinutes().zf(2);
            case "ss":
                return r.getSeconds().zf(2);
            case "a/p":
                return r.getHours() < 12 ? "오전" : "오후";
            default:
                return t;
        }
    })
}, String.prototype.string = function (t) {
    for (var e = "", r = 0; r++ < t;) e += this;
    return e;
}, String.prototype.zf = function (t) {
    return "0".string(t - this.length) + this;
}, Number.prototype.zf = function (t) {
    return this.toString().zf(t);
};

function maxLengthCheck(object) {
    if (object.value.length > object.maxLength) {
        object.value = object.value.slice(0, object.maxLength);
    }
}


function selectRadio (num) {
    var radio = document.getElementsByClassName('radio')
    var select = document.getElementsByClassName('select')

    for (var i = 0; i < radio.length; i++) {
        radio[i].style.display = 'block'
        select[i].style.display = 'none'
    }

    radio[num].style.display = 'none'
    select[num].style.display = 'block'
}

function locationHashChanged() {

    console.log("You're visiting a cool feature!");

}

function updateSession() {

}

/**
 * 2021.06.24 고금필 : 작성
 *
 * 최상단 이동
 */
let totheTop = function() {
    $(window).scrollTop(0)
}

/**
 * 2021.08.02 고금필 로딩 이미지 표시
 */
function showLoadingWithMask() {
    if ( $('#loadingImg') && $('#loadingImg').css('display') === 'none' ) {
        $('#mask, #loadingImg').css('display','block');
    } else {
        //화면에 출력할 마스크를 설정해줍니다.
        var loadingImg = '';

        loadingImg += "<div id='loadingImg' class='modal fade show'>";
        loadingImg += "  <div class='modal-dialog modal-dialog-centered'>"
        loadingImg += "      <div id='loadingImg-container' style='position: relative; display: -ms-flexbox; display: flex; -ms-flex-direction: column; flex-direction:column; width: 100%'>"
        loadingImg += "         <div style='position: relative; width: 100%; text-align: center;'>"
        loadingImg += "             <img src='/img/loading.gif' style='width: 50px; height:50px;'>"
        loadingImg += "         </div>"
        loadingImg += "         <div class='mt-3' style='position: relative; width: 100%; text-align: center;'>"
        loadingImg += "             <span style='font-size: 20px;font-weight: bold;color: floralwhite;'>잠시만 기다려주세요.</span>"
        loadingImg += "         </div>"
        loadingImg += "      </div>"
        loadingImg += "  </div>"
        loadingImg += "</div>"
        loadingImg += "<div id='mask' class='modal-backdrop fade show'></div>"

        //화면에 레이어 추가
        $('body').append(loadingImg)

        //로딩중 이미지 표시
        $('#loadingImg').show();
    }
}
/**
 * 2021.08.02 고금필 로딩 이미지 닫기
 */
function closeLoadingWithMask() {
    $('#mask, #loadingImg').hide();
}

function convertShortUrl(url) {
    return new Promise((resolve, reject) => {
        let FormData = new Object();
        FormData.uri = '/api/v1/affiliate/shorturl';

        if ( url !== undefined ) {
            FormData.target = url;
        } else {
            FormData.target = window.location.href;
        }
        $.post("/api/execute", FormData, function (jqXHR) {
            if (jqXHR.msg === 'SUCCESS') {
                resolve(jqXHR)
            } else {
                modal("오류로인해 실패하였습니다","ok","hide")
                reject(new Error("Request is failed"));
            }
        }, 'json' /* xml, text, script, html */)
            .done(function (jqXHR) {
                console.log(1)
            })
            .fail(function (jqXHR) {
                console.log(2)
            })
            .always(function (jqXHR) {
            });
    });
}

/**
 * 2021.08.10 고금필 MobileWebShare Function
 *
 * @param title Message Title
 * @param text Message Body
 * @param url Message Url
 * @returns {Promise<void>}
 */
function mobileWebShare(title,text,url,mid) {
    convertShortUrl(url).then(async function (data) {
        if (typeof navigator.share !== "undefined") {

            await navigator.share({
                title: title,
                text: text,
                url: data.result,
            }).then(() => console.log('완료'))
                .catch((error) => console.log(error));
        } else {
            //Not Support Web Share!!
            modal(title + "|" + text + "|" + data.result + "|" + mid ,"share");
        }
    }).catch(function (err) {
        console.log(err)
    });
}

function copyToClipboard (txt) {
    function handler (event)
    {
        event.clipboardData.setData('text/plain', txt);
        event.preventDefault();
        document.removeEventListener('copy', handler, true);
    }
    document.addEventListener('copy', handler, true);
    document.execCommand('copy');

    $('.modal.share').modal('hide');
    modal('주소가 복사되었습니다.','ok','hide');
}

function shareTo (title,text,url,sns) {
    let shareLink;
    let popupTitle;
    let popupOptions;

    switch (sns) {
        case 'facebook':
            shareLink = "http://www.facebook.com/sharer.php?u=" + encodeURIComponent(url)
            popupTitle = "페이스북 공유하기"
            popupOptions = "top=10,left=10,width=700,height=300,menubar=no,toolbar=no,resizable=yes";
            break;
        case 'twitter':
            shareLink = "https://twitter.com/intent/tweet?text="+ text +"&url=" + encodeURIComponent(url)
            popupTitle = "트위터 공유하기"
            popupOptions = "top=10,left=10,width=700,height=550,menubar=no,toolbar=no,resizable=yes";
            break;
        default:
            $('.modal.share').modal('hide');
            console.log('Wrong Prams Error');
    }
    window.open(shareLink, popupTitle, popupOptions)
}

function shareToKakao(title,text,url,mid) {
    console.log(title)
    console.log(text)
    console.log(mid)
    let FormData = new Object()
    let imgUrl;
    FormData.Member_ID = mid;
    FormData.uri = '/api/v1/member/memberinfo'
    $.post("/api/execute", FormData, function (jqXHR) {
        if (jqXHR.msg !== 'NOT FOUND') {
            switch (title) {
                case '[제이패스 - 중개자스토어]':
                    imgUrl = 'https://jpass.jeju.kr' + jqXHR[2][0].CoverFilepath + jqXHR[2][0].CoverFilename;
                    url = 'https://jpass.jeju.kr/affiliate/showstore/' + mid;
                    break;
                case '[제이패스 - 상품]':
                    imgUrl = url;

                    let spUrl = window.location.href.split('/');

                    url = ('https://jpass.jeju.kr/user/search/'+ spUrl[5]).replace('id','itemid');
                    break;
                default:
                    url = 'https://jpass.jeju.kr';
            }
            try {
                // 카카오링크 버튼 생성
                Kakao.Link.sendDefault({
                    // container: '#share-Kakaotalk', // 카카오공유버튼ID
                    objectType: 'feed',
                    content: {
                        title: title, // 보여질 제목
                        description: text, // 보여질 설명
                        imageUrl: imgUrl, // 콘텐츠 URL
                        link: {
                            mobileWebUrl: url,
                            webUrl: url
                        },
                    },
                    buttons: [
                        {
                            title: '웹으로 이동하기',
                            link: {
                                mobileWebUrl: url,
                                webUrl: url,
                            },
                        },
                    ]
                });
            }
            catch (e) {
                $('.modal.share').modal('hide');
                modal('오류가 발생하였습니다.<br>팝업차단이되있을경우 해제후 다시시도해주세요.','ok','hide')
            }
        } else {
            $('.modal.share').modal('hide');
            modal("오류가 발생하였습니다. 다시시도해주세요","ok","hide")
        }
    }, 'json' /* xml, text, script, html */)
        .done(function (jqXHR) {
            console.log(1)
        })
        .fail(function (jqXHR) {
            console.log(2)
        })
        .always(function (jqXHR) {
        });
}

var convert = function (value) {
    let inputStr = value
    let outputHtml = "<p>";
    let blankPattern = /^\s+|\s+$/g;

    for (var i = 0; i < inputStr.length; i++) {
        if (inputStr.substr(i, 1) == '\n') {
            outputHtml += "<br>";
        } else if (inputStr.substr(i, 1) === ' ') {
            outputHtml += "&nbsp;";
        } else if (blankPattern.test(inputStr.substr(i, 1))) {
            outputHtml += "&nbsp;";
        } else if (inputStr.substr(i, 1) == '&') {
            outputHtml += "&amp;";
        } else if (inputStr.substr(i, 1) == '"') {
            outputHtml += "&quot;";
        } else if (inputStr.substr(i, 1) == '>') {
            outputHtml += "&gt;";
        } else if (inputStr.substr(i, 1) == '<') {
            outputHtml += "&lt;";
        } else {
            outputHtml += inputStr.substr(i, 1);
        }
    }
    outputHtml += "</p>";
    return outputHtml; // html로 변환된 데이터
}

var decodeConvert = function (value) {
    let inputStr = value
    let outputHtml = "";
    outputHtml = inputStr.replace(/<p>/gi,'');
    outputHtml = outputHtml.replace(/<\/p>/gi,'');
    outputHtml = outputHtml.replace(/&nbsp;/gi,' ');
    outputHtml = outputHtml.replace(/<br>/gi, '\n');
    outputHtml = outputHtml.replace(/&lt;/gi, '<');
    outputHtml = outputHtml.replace(/&gt;/gi, '>');
    return outputHtml;
}

document.writeln('<script type="text/javascript" src="/js/kakao.min.js"></script>')

$(document).ready(function(){

    if ( !Kakao.isInitialized() ) {
        Kakao.init('f3b376251589fd13ae53cd8c6df6bafd');
    }

    $('input').on('input',function(){
        var $th = $(this);

        emojiInput= isEmoji($th.val());
        if (emojiInput==true) {
            var str = $th.val();
            $th.val(str.replace(/([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g, ''));
        }
    });

    console.log('origin--',window.location.origin)
})

// window.onhashchange = locationHashChanged(); 2021.07.16 고금필 레거시