var langs = new Array('ko-KR','en-US')
var msgs = {
    "nomemberintro":"한줄소개를 입력해주세요",
    "nophone":"전화번호를 입력해주세요",
    "noaddr":"주소를 입력해주세요",
    "noaddretc":"상세주소를 입력해주세요",
    "nopostcode":"우편번호를입력해주세요",
    "nopresident":"대표자 성명을 확인해주세요 (공백 또는 특수문자 불가)", 
    "nobank":"은행을 선택해주세요",
    "notaxemail":"세금계산서 이메일을 입력해주세요",
    "nobankimg":"통장사본을 업로드해주세요",
    "nobank":"은행을 선택해주세요",
    "noidcard":"신분증사본을 업로드해주세요",
    "noaccountholder":"예금주를 입력해주세요",
    "nobankccount":"계좌번호를 입력해주세요",
    "nobankccount":"계좌번호를 입력해주세요",
    "nocategory":"업종 카테고리를 선택해주세요",
    "nobizitem":"종목을 입력해주세요",
    "nobiztype":"업태를 입력해주세요",
    "nobizcode":"사업자등록번호 입력해주세요",
    "nobizcodefile":"사업자등록증 사본을 업로드해주세요",
    "nocompanyname":"상호명을 입력해주세요",
    "nocorpcode":"법인등록번호를 입력해주새요",
    "nocorpstamp":"법인인감증명서 사본을 업로드해주새요",
    "affsupauthsucc":"신청이 완료 되었습니다.",
    "affsupauthfail":"신청에 실패 하였습니다.",
    "nosnsname":"SNS가 선택되지 않았습니다",
    "nosnsurl":"URL이 비었습니다.",
    "confirmchange":"변경하시겠습니까?",
    "confirmbtn":"확인",
    "yesbtn":"네",
    "nobtn":"아니오",
    "authok":"인증에 성공하였습니다.",
    "authfail":"인증에 실패하였습니다.",
    "sendsuccess":"인증 번호가 전송되었습니다.",
    "sendfail":"인증 번호 전송에 실패하였습니다.",
    "nococeo":"공동사업자를 입력해주세요.",
    "nomainpostcode":"본점 우편번호를 입력해주세요.",
    "nomainaddr":"본점 주소를 입력해주세요.",
    "nomainaddretc":"본점 상세주소를 입력해주세요.",
    "nomainphone":"본점 전화번호를 입력해주세요.",
    "nomatchaddr":"주소가 동일하지 않습니다.",
    "sugestsuc":"제안이 완료되었습니다.",
    "sugestfail":"오류가 발생하였습니다.",
    "noname":"이름이 비어있거나 형식에 맞지 않습니다.",
    "entermsg" :"메시지를 입력해주세요.",
    "nophonenum" :"전화번호가 비어있거나 형식에 맞지 않습니다.",
    "noauthcode" : "인증번호가 비어있거나 형식에 맞지 않습니다.",
    "noemail":"이메일이 비어있거나 형식에 맞지 않습니다.",
    "noid":"아이디가 비어있거나 형식에 맞지 않습니다.",
    "noaccount":"아이디가 사용중이거나 형식에 맞지 않습니다.",
    "affsupexistphonenum":"사용 할 수 없는 번호입니다.<br>( 사유 : 중개자, 공급자 사용번호 )",
    "alreadyusedphonenum":"이미 사용중인 번호입니다.<br>기존 인증을 해제하고 계속 진행하시겠습니까?",
    
    // 2021.08.04 txt 템플릿추가 이병주
    "support":'',
    "supportPgf":'환불오류가 발생하였습니다. <br /> 제이패스 고객센터로 연락 바랍니다. <br />(Tel: 064-900-3658)',
    "supportPgfDel":'환불오류가 발생한 배송건입니다. <br /> 제이패스 고객센터로 연락 바랍니다. <br />(Tel: 064-900-3658)'

    //#region 프로시저관련 오류 ( 네이밍규칙 -> 대문자, 어떤프로시저에 어떤오류코드(숫자)인지 주석으로표시 )
    ,
    "DEFAULTFAILMSG": '예기치 못한 오류로 실패하였습니다 <br />고객센터: 064-900-3658 으로 연락주시기 바랍니다. <br>운영시간: 월~금 09:00~18:00', // 각 프로시저 -1 에러 및 알수없는 오류 처리

    "RECORDCHECKFAIL": '해당내역이 없습니다.', // console.log('sbb_CompleteReturn -101')
    "MODECHECKFAIL": '실행가능 회원모드가 아닙니다.', // console.log('sbb_CompleteReturn -101')
    "IDCHECKFAIL": '실행가능 회원번호가 아닙니다.', // console.log('sbb_CompleteReturn -101')
    "STATUSFAIL": '실행가능 상태가 아닙니다.', // console.log('sbb_CompleteReturn -101')
    "PRICEFAIL": '가능한 금액이아닙니다.(주문금액초과요청)', // console.log('sbb_CompleteReturn -101')
    //#endregion
}

function getLang() {
    var userLang = navigator.language || navigator.userLanguage;
    return userLang;
}

function modalmsg (str){
    if( msgs[str]  )
        return msgs[str]
    else
        return str
}