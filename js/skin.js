let hostname = window.location.hostname // ex, visitgotjawal.kr jpass.jeju.kr
let main_title_jpass = document.getElementById("main_title_jpass") // id = 이미지명, 메인, 스토어 로고  
let jpass_logo = document.getElementById("jpass_logo") // 

var setStoreLogo = function () { // 공급자/중개자 스토어, 메인 로고
    if( hostname == 'jpass.jeju.kr'  ) {
        main_title_jpass.setAttribute('src','/img/main_title_jpass.png')
        main_title_jpass.style.width = "100px";
        main_title_jpass.style.display = 'inline'
    } else if ( hostname == 'visitgotjawal.kr' ) {
        main_title_jpass.style.display = 'none'
    } else {
        main_title_jpass.setAttribute('src','/img/main_title_jpass.png')
        main_title_jpass.style.width = "100px";
        main_title_jpass.style.display = 'inline'
    }
}

var setLoginLogo = function () { // 로그인 로고
    if( hostname == 'jpass.jeju.kr'  ) {
        jpass_logo.setAttribute('src','/img/jpass_logo.png')
        jpass_logo.style.width = "100px";
        jpass_logo.style.display = 'inline'
    } else if ( hostname == 'visitgotjawal.kr' ) {
        jpass_logo.style.display = 'none'
    } else {
        jpass_logo.setAttribute('src','/img/jpass_logo.png')
        jpass_logo.style.width = "100px";
        jpass_logo.style.display = 'inline'
    }
}

// = jquery ready function
document.addEventListener("DOMContentLoaded", function(){
    if( main_title_jpass != null) {
        setStoreLogo()
    }

    if (jpass_logo != null ) {
        setLoginLogo()
    }
});
    
    