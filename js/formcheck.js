

function emailCheck(input) {

    if (typeof input  == 'string'){
        var email = input
    }else{
        var email = input.value
    }
    // var email = input.value 
    var regExp = /^[a-z0-9][a-z0-9-_.]*@[a-z0-9]([-_.]?[a-z0-9])*[.][a-z]{2,3}$/g;
    if (email.match(regExp) != null) {
        return true; 
    } else {        
        return false;
    }
}

function idCheck(input) {
    if (typeof  input == 'string') {
        var id = input
    } else {
        var id = input.value
    }
    let regExp = /^[a-z0-9][a-z0-9\_\-]{5,64}/g
    if(id.match(regExp) != null) {
        return true
    } else {
        return false
    }
}

function passCheck(input){
// console.log(typeof input)
    if (typeof input  == 'string'){
        var pass = input
    }else{
        var pass = input.value
    }
  
    var regExp = /(?=.*\d)(?=.*[a-z]).{8,15}/i   
    if (pass.match(regExp) != null) {
        return true; 
    } else {
        return false;
    }
}

function removeSpacial(obj){
    //함수를 호출하여 특수문자 검증 시작.
    var str = obj.value;
    var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
    if(regExp.test(str)){
        var t = str.replace(regExp, "");
        obj.value = t
    }
   
}

function checkWaybillNum(input) {
    var waybillNum = autoHypenWaybill(input.value)

    if(waybillNum.length>16){
        waybillNum = new String(waybillNum).slice(0, 16 - waybillNum.length );
    }
    var regExp = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
    if (waybillNum.match(regExp) != null) {
        input.value = waybillNum
        return true;      
    } else {
        input.value = waybillNum
        return false;
    }
}

function telPhoneCheck(input) {
    var phone = autoHypenPhone(input.value)
    if(phone.length>13){
        phone = new String(phone).slice(0, 13 - phone.length );
    }
    // var regExp = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$/;
    var regExp = /^[0-9]{2,3}-[0-9]{3,4}-[0-9]{4}/;

    if (phone.match(regExp) != null) {
        input.value = phone
        return true;
    } else {
        input.value = phone
        return false;
    }
}

function phoneCheck(input){
    var phone = autoHypenPhone(input.value)
    if(phone.length>13){
        phone = new String(phone).slice(0, 13 - phone.length );
    }
    var regExp = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
    if (phone.match(regExp) != null) {
        input.value = phone
        return true;      
    } else {
        input.value = phone
        return false;
    }
}

var nameCheck = function(obj){
    const name = $(obj).val();

    var regExp = /^[가-힣|a-z|A-Z\*]+$/
    if (name.match(regExp) != null) {
        return true;
    } else {
        return false;
    }
}

var checkName = function(obj){
    const name = $(obj).val();
    var regExp = /^[가-힣|a-z|A-Z|\*]+$/
    if (name.match(regExp) != null) {
        return true;
    } else {
        return false;
    }
}

// 수령인 ( 정팀장님 요처응로 '_'제외 특수문자,숫자 공백등 모두허용 , 2021.08.04 이병주 )
var checkReqName = function(obj){
    const name = $(obj).val();
    var regExp = /^[가-힣|a-z|A-Z|\W|0-9\*]+$/
    if (name.match(regExp) != null) {
        return true;
    } else {
        return false;
    }
}


function spaceCheck(val){
    if (val != '') {        
        return true; 
    } else {
        return false;
    }
}

var removeChar = function(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    if ( keyID == 8 || keyID == 46 || keyID == 37 ) {
        return;
    }
    else {
        event.target.value = event.target.value.replace(/[^0-9]/gi, "");
    } 
}

/**
 * Check isNumber
 *
 * @param param Data
 * @param isObject param Data가 단순 value인지 <input>태그와같은 Object인지 ( true : Object, false : value )
 */
function isNumber(param, isObject) {
    const reqExp = /^[0-9]*$/g //숫자체크 정규식
    if(isObject === true) { //
        let val = param.val
        if(reqExp.test(val)) {
            return true
        } else {
            return false
        }
    } else {
        if(reqExp.test(param)) {
            return true
        } else {
            return false
        }
    }
}

function inputBizNumber(obj) {
    var number = obj.value.replace(/[^0-9]/gi, "");
    var biz = "";

    if (number.length < 4) {
        return number;
    } else if (number.length < 6) {
        biz += number.substr(0, 3);
        biz += "-";
        biz += number.substr(3, 2);

    } else {
        biz += number.substr(0, 3);
        biz += "-";
        biz += number.substr(3, 2);
        biz += "-";
        biz += number.substr(5, 5);
    }

    if (number.length > 11) {
        obj.value = biz.slice(0, -1);
    } else {
        obj.value = biz;
    }
}

function inputCorNumber(obj) {
    var number = obj.value.replace(/[^0-9]/gi, "");
    var biz = "";
    if (number.length < 5) {
        return number;
    } else if (number.length < 6) {
        biz += number.substr(0, 6);
    } else {
        biz += number.substr(0, 6);
        biz += "-";
        biz += number.substr(6, 7);
    }

    if (number.length > 14) {
        obj.value = biz.slice(0, -1);
    } else {
        obj.value = biz;
    }
}

function telCheck(input) {
    // console.log(input.value);
    var number = input.value.replace(/[^0-9]/gi, "");
    var tel = "";
    var seoul = 0;

    if (number.substr(0, 2).indexOf('02') == 0) {
        seoul = 1;
    }

    if (number.length < (4 - seoul)) {
        return number;
    } else if (number.length < (6 - seoul)) {
        tel += number.substr(0, (3 - seoul));
        tel += "-";
        tel += number.substr(3 - seoul);
    } else {
        tel += number.substr(0, (3 - seoul));
        tel += "-";
        tel += number.substr((3 - seoul), 3);
        tel += "-";
        tel += number.substr(6 - seoul, 4);
    }

    if (number.length > 11) {
        input.value = tel.slice(0, -1);
    } else {
        input.value = tel;
    }
}

function onlyNumber(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    if (keyID == 8 || keyID == 46 || keyID == 37) {
        return;
    } else {
        event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
}

function numberAndHyphen(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    if (keyID == 8 || keyID == 46 || keyID == 37) {
        return;
    } else {
        event.target.value = event.target.value.replace(/[^0-9-]/g, "");
    }
}

function autoHypenWaybill(str){

    str = str.replace(/[^0-9]/g, '');
    var tmp = '';
  
    if( str.length < 4){
      return str;
    }else if(str.length < 7){
      tmp += str.substr(0, 3);
    //   tmp += '-';
      tmp += str.substr(3);
      return tmp;
    }else if(str.length < 11){
      tmp += str.substr(0, 3);
    //   tmp += '-';
      tmp += str.substr(3, 3);
    //   tmp += '-';
      tmp += str.substr(6);
      return tmp;
    }else{
      tmp += str.substr(0, 3);
    //   tmp += '-';
      tmp += str.substr(3, 4);
    //   tmp += '-';
      tmp += str.substr(7);
      return tmp;
    }
    return str;
  }

function autoHypenPhone(str){

  str = str.replace(/[^0-9]/g, '');
  var tmp = '';
    

  if( str.length < 4){
    return str;
  }else if(str.length < 7){
    tmp += str.substr(0, 3);
    tmp += '-';
    tmp += str.substr(3);
    return tmp;
  }else if(str.length < 11){
    tmp += str.substr(0, 3);
    tmp += '-';
    tmp += str.substr(3, 3);
    tmp += '-';
    tmp += str.substr(6);
    return tmp;
  }else{
    tmp += str.substr(0, 3);
    tmp += '-';
    tmp += str.substr(3, 4);
    tmp += '-';
    tmp += str.substr(7);
    return tmp;
  }
  return str;
}

var insertHypen = function(obj){

    str = obj.value.replace(/[^0-9]/g, '');
    var tmp = '';

        // case: 15--, 16--, 18--
    if(str.substring(0,2) == 15 || str.substring(0,2) == 16 || str.substring(0,2) == 18){
        if(str.length < 5){
           return str
        } else {
            tmp += str.substring(0, 4)
            tmp += '-'
            tmp += str.substring(4, 8)
        } 

        if (str.length > 9) {
            obj.value = tmp.slice(0, -1);
        } else {
            obj.value = tmp;
        }
    }

    // case: 02
    else if(str.substring(0,2) == 02){
        if(str.length < 3){
            return str
        } else if (str.length < 6){
            tmp += str.substring(0, 2)
            tmp += '-'
            tmp += str.substring(2, 5)
        } else if (str.length < 10 ) {
            tmp += str.substring(0, 2)
            tmp += '-'
            tmp += str.substring(2, 5)
            tmp += '-'
            tmp += str.substring(5, 9)
        } else {
            tmp += str.substring(0, 2)
            tmp += '-'
            tmp += str.substring(2, 6)
            tmp += '-'
            tmp += str.substring(6, 10)
        }

        if (str.length > 12) {
            obj.value = tmp.slice(0, -1);
        } else {
            obj.value = tmp;
        }
    }
    //그외
    else {
        if(str.length < 4){
            return str
        } else if (str.length < 7){
            tmp += str.substring(0, 3)
            tmp += '-'
            tmp += str.substring(3, 6)
        } else if (str.length < 11) {
            tmp += str. substring(0, 3)
            tmp += '-'
            tmp += str.substring(3, 6)
            tmp += '-'
            tmp += str.substring(6, 10)
        } else {
            tmp += str. substring(0, 3)
            tmp += '-'
            tmp += str.substring(3, 7)
            tmp += '-'
            tmp += str.substring(7, 11)
        }

        if (str.length > 13) {
            obj.value = tmp.slice(0, -1);
        } else {
            obj.value = tmp;
        }
    }
}
