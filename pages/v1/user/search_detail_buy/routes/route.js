const { isFunction } = require("util");


module.exports = function(ver, app,  runmode, iamporter ) {	
	const CryptoJS = require("crypto-js")

	function getSignData(str) {
		var encrypted = CryptoJS.SHA256(str);
		return encrypted;
	}
	
	var render = require('../../../../../util/render');
	var request = require('request');
	var path = require('path');
	var page = path.resolve(__dirname, '../view/view');

	var thispath = path.resolve(__dirname, '../').replace(/\\/gi, "/").replace(/\\/gi, "/");
	var originPath = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") ,'') 

	var url = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") ,'').replace(/_/gi, "/")
	// url = url.replace('_','/')
	// url = url.replace('_','/')
	thispath = path.resolve(__dirname, '../../').replace(/\\/gi, "/"); 
	var thismode = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/")+'/','').replace(/\\/gi, "/"); 

	/* 나이스패이 키 설정 */
	 
	const merchantKey = global_nice_merchantKey;
	const merchantID = global_nice_merchantID;

	// app.get(url, function(req,res) {
	// 	var data = new Object();
	// 	data.Member_ID = req.session.Member_ID
	// 	// data.username = req.session.AccountID
	// 	data.Contract_ID =  req.query.Contract_ID
	// 	data.Qty =  req.query.qty

	// 	var Item_ID = req.query.itemid

	// 	if(req.session.Item_ID)	
	// 		delete req.session.Item_ID

	// 	if(req.session.Contract_ID) 
	// 		delete req.session.Contract_ID
				
	// 	if (req.body.selday) {
	// 		var selday = req.body.selday
	// 	}
		
	// 	if (req.body.seq) {
	// 		var seq = req.body.seq
	// 	}

	// })

	app.post(url,function(req,res) {

		var sess = req.session;
		sess.updateSession = true;

		console.log('search detail session', req.session)

        if(!req.session.AccountID) { // 로그인이 안되어 있을때
            res.redirect('/member/login');
            return;
        }  else if (req.session.MemberMode_CD == 'SUP'){ // 공급자 모드 일때
             res.redirect('/supplier/main');//공급자 모드 일때 이동주소
            return;
        }  else if (req.session.MemberMode_CD == 'AFF'){ // 중개자 모드 일때
             res.redirect('/affiliate/main');//중개자 모드 일때 이동주소
            return;
        } /* else if (req.session.MemberMode_CD == 'USR'){ //사용자 모드 일때
             res.redirect('/user/main');//사용자 모드 일때 이동주소
            return;
		} */

		var data = new Object();
		data.Member_ID = req.session.Member_ID
		data.username = req.session.AccountID
		data.Contract_ID =  req.query.Contract_ID
		data.Qty =  req.query.qty

		var Item_ID = req.query.itemid
		var selday
		var seq
		var timename = req.body.timename
		var itemoptionContractID = []
		var itemoptionPriceArr = []
		var itemoptionQtyArr = []
		var totalPrice
		var totalPriceATR = 0

		if(req.session.Item_ID)	
			delete req.session.Item_ID

		if(req.session.Contract_ID) 
			delete req.session.Contract_ID
				
		if (req.body.selday) {
			selday = req.body.selday
		}
		
		if (req.body.seq) {
			seq = req.body.seq
		}

		if (req.body.ItemType_CD == 'ATR') {
			if (req.body.totalPrice) {
				totalPrice = req.body.totalPrice
			} else {
				totalPrice = 0
			}
			if (req.body.itemoptionContractID) {
				itemoptionContractID = req.body.itemoptionContractID
			} else {
				itemoptionContractID = []
			}
			if (req.body.itemoptionPriceArr) {
				itemoptionPriceArr = req.body.itemoptionPriceArr
			} else {
				itemoptionPriceArr = []
			}
			if (req.body.itemoptionQtyArr) {
				itemoptionQtyArr = req.body.itemoptionQtyArr
			} else {
				itemoptionQtyArr = []
			}
		}

		// console.log("totalPrice: ", totalPrice)

		// console.log("itemoptionContractID: ", itemoptionContractID)
		// console.log("itemoptionPriceArr:", itemoptionPriceArr)
		// console.log("itemoptionQtyArr:", itemoptionQtyArr)

		res.cookie("PageType", "Single")

		data.Page = 'Single'
		data.uri = '/api/v1/item/getfinalorderpage'

		let options = {
			uri: 'http://127.0.0.1:3000/api/execute',
			method: 'POST',
			body: data,
			json: true
		};

		request.post(options, function (err, httpResponse, body) {

			// console.log('body',body.result) 

			var title = '제이패스 - 구매정보';
			var topnav_left = '<span class="icon before align-middle" onclick="move(getRedirectURL(1))">before</i>';
			var topnav_title = '구매정보';
			var topnav_right = '<span class="text _ee7499">결제</span>';
			var isbottomnav = false;
	
			var date = new Date()

			var yyyyMMddhhmmss = date.getFullYear().toString()
			yyyyMMddhhmmss += (date.getMonth() + 1).toString().padStart(2 , '0')
			yyyyMMddhhmmss += date.getDate().toString().padStart(2 , '0')
			yyyyMMddhhmmss += date.getHours().toString().padStart(2 , '0')
			yyyyMMddhhmmss += date.getMinutes().toString().padStart(2 , '0')
			yyyyMMddhhmmss += date.getSeconds().toString().padStart(2 , '0')

			var ediDate = yyyyMMddhhmmss
			var totalAmount = body.result[4][0].TotalAmount
			
			var returnURL = 'https://jpass.jeju.kr/member/payresult' //hostname+'/member/payresult'; pay.js 에서 재작성
			var moid = 'AUP_' + new Date().getTime()
			var buyerName = '구매자';
			var buyerEmail = '';
			var buyerTel = '00000000000';

			// console.log("Filepath: ", body.result[2][0].Filepath)
			// console.log("Filename: ", body.result[2][0].Filename)

			if (req.body.ItemType_CD == 'ATR') {
				for (var i = 0; i < itemoptionContractID.length; i++) {
					if (itemoptionQtyArr[i] > 0) {
						totalPriceATR += (itemoptionPriceArr[i] * itemoptionQtyArr[i])
					}
				}
			}

			if (req.body.ItemType_CD == 'ATR') {
				var params = {
					title,
					topnav_left,
					topnav_title,
					topnav_right,
					thismode,
					isbottomnav,
					memberinfo:body.result[0],
					supinfo:body.result[1],
					itempic:body.result[2],
					iteminfo:body.result[3],
					itempayinfo:body.result[4],
					addrinfo:body.result[5],
					ItemType_CD: req.body.ItemType_CD,
					Contract_ID:data.Contract_ID,
					Item_ID:Item_ID,
					itemoptionContractID: itemoptionContractID,
					itemoptionPriceArr: itemoptionPriceArr,
					itemoptionQtyArr: itemoptionQtyArr,
					totalPrice: totalPrice,
					totalPriceATR: totalPriceATR,
					qty:data.Qty,
					selday:selday,
					seq:seq,
					timename:timename,
					merchant_uid : moid,
					buyerName : buyerName,
					buyerEmail : buyerEmail,
					buyerTel : buyerTel,
					merchantID: merchantID,
					ediDate: ediDate,
					hashString : getSignData(ediDate + merchantID + totalPriceATR + merchantKey).toString(),
					global_PgPassGrade : global_PgPassGrade,
					returnURL: returnURL
				};
				
			} else {
				var params = {
					title,
					topnav_left,
					topnav_title,
					topnav_right,
					thismode,
					isbottomnav,
					memberinfo:body.result[0],
					supinfo:body.result[1],
					itempic:body.result[2],
					iteminfo:body.result[3],
					itempayinfo:body.result[4],
					addrinfo:body.result[5],
					ItemType_CD: req.body.ItemType_CD,
					Contract_ID:data.Contract_ID,
					Item_ID:Item_ID,
					qty:data.Qty,
					selday:selday,
					seq:seq,
					timename:timename,
					merchant_uid : moid,
					buyerName : buyerName,
					buyerEmail : buyerEmail,
					buyerTel : buyerTel,
					merchantID: merchantID,
					ediDate: ediDate,
					hashString : getSignData(ediDate + merchantID + totalAmount + merchantKey).toString(),
					global_PgPassGrade : global_PgPassGrade,
					returnURL: returnURL
				};
			}
			
			render.page(res,req,page,params);
		})

	});

	// get방식 요청시 에러페이지 페이지 렌더링 2021.05.14 이병주 
	app.get(url,function(req,res) {
		let errorPageDir = '/user/main' // 통합 에러페이지를 제작 할 경우 여기서 디렉토리수정
		
		res.redirect(errorPageDir) // 메인페이지로 보냄
		return
	})
};