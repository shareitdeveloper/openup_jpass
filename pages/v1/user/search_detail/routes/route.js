module.exports = function (ver, app, runmode, iamporter) {
	var render = require('../../../../../util/render');
	var request = require('request');
	var path = require('path');
	var page = path.resolve(__dirname, '../view/view');
	var thispath = path.resolve(__dirname, '../').replace(/\\/gi, "/").replace(/\\/gi, "/");
	var originPath = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") ,'')
	var url = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/"), '').replace(/_/gi, "/")
 
	thispath = path.resolve(__dirname, '../../').replace(/\\/gi, "/");
	var thismode = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") + '/', '').replace(/\\/gi, "/");

	app.get(url, function (req, res) {

		var data = new Object();

		data.Member_ID = req.session.Member_ID;
		data.username = req.session.AccountID;

		data.mode = 'USR'
		data.uri = '/api/v1/member/changemode'

		let options = {
			uri: 'http://127.0.0.1:3000/api/execute',
			method: 'POST',
			body: data,
			json: true
		};

		request.post(options, function (err, httpResponse, body) {

			// if (req.session.MemberMode_CD == 'SUP'){ // 공급자 모드 일때
			//      res.redirect('/supplier/main');//공급자 모드 일때 이동주소
			//     return;
			// }  else if (req.session.MemberMode_CD == 'AFF'){ // 중개자 모드 일때
			//      res.redirect('/affiliate/main');//중개자 모드 일때 이동주소
			//     return;
			// } 

			var sess = req.session
			var isafflink = false

			if (req.query.sms && req.query.sms == 'true') {
				isafflink = true
			}
			/**
			 * 2021.05.28 고금필 : req.query.Contract_ID, itemid 숫자이외 문자 처리 정규식 추가
			 */
			req.session.Contract_ID = req.query.Contract_ID.replace(/[^0-9]/g,"")
			req.session.Item_ID = req.query.itemid.replace(/[^0-9]/g,"")

			var data = new Object();
			data.Member_ID = req.session.Member_ID
			data.MemberMode_CD = sess.MemberMode_CD
			data.Item_ID = req.session.Item_ID
			data.Contract_ID = req.session.Contract_ID
			data.username = req.session.AccountID

			data.uri = '/api/v1/item/getitemdetail'

			let options = {
				uri: 'http://127.0.0.1:3000/api/execute',
				method: 'POST',
				body: data,
				json: true
			};

			/**
			 * 2021.05.28 고금필 : 주석 추가, API return value 정리
			 *
			 * /api/v1/member/memberinfo request.post 실행
			 *
			 * [result info]
			 * body[0][0] - 상품사진 ( index마다 상품 사진 return )
			 * body[1][0] - 판매자 프로필
			 * body[2][0] - 상품 계약정보
			 * body[3][0] - 계약정보
			 * body[4][0] - 상품상세사진
			 * body[5][0] - sbb_ 프로시저 line253 ~ 참고
			 * body[6][0] - sbb_ 프로시저 line253 ~ 참고
			 *
			 * call Api uri : api/v1/item/getitemdetail
			 */
			request.post(options, async function (err, httpResponse, body) {
				// console.log("debug1 : ",body.result)
				/**
				 * 2021.05.28 고금필 : itemid 에러 예외처리
				 * 사용자가 아무런 의미없는 주소를 입력하여 에러가발생한것이 아니기때문에 에러발생시 예외처리 redirect 경로를 /error/notfound -> /(메인)으로 변경
				 * itemid 에대한 Contract_ID가 잘못된경우 에러 예외처리
				 */
				if(body.result[0] == '' || body.result[2] == ''){ res.redirect("/"); return; }

				/**
				 * 2021.02.17 이병주 : 예약상품이 아닐때 초기값처리 (itemoption)
 				 */
				if(!body.result[6]){
					body.result[6] = ''
				}

				/**
				 * 2021.05.28 고금필 : 알 수 없는코드 주석처리
				 */
				//data.Item_ID = data.Item_ID //???

				data.uri = '/api/v1/item/getresvlist'

				/**
				 * call Api uri : /api/v1/item/getresvlist
				 */
				request.post(options, async function (err, httpResponse, leftoverno) {
					
					// console.log('resvlist console')

					// for (var i = 0; i < leftoverno.result[0].length; i++) {
					// 	console.log("LeftOverQty: ", leftoverno.result[0][i].LeftOverQty)
					// }

					var title = '제이패스 - 상품상세내역';		
					// var topnav_left = '<span class="icon before align-middle" onclick="move(\'/affiliate/showstore/'+body.result[2][0].Affiliate_ID+'\')">before</span>';
					var topnav_left = '<span class="icon before align-middle" onclick="move(getRedirectURL(0))">before</span>';
					var topnav_title = '상품상세내역';
					var topnav_right = '';
					var isbottomnav = false;

					if (isafflink) {
						//console.log('중개자링크')
						req.session.SendAffiliate_ID = body.result[2][0].Affiliate_ID
					}

					if (isafflink && sess.Member_ID) {
						data.FriendType_CD = 'PLU'
						data.Member_ID = sess.Member_ID
						data.Member_FID = sess.SendAffiliate_ID
						data.FriendStatus_CD = 'ACC'
						data.username = sess.AccountID
						data.MemberGrade_CD = sess.MemberGrade_CD
						data.uri = '/api/v1/member/reqfriend'
						request.post(options, async function (err, httpResponse, rst) {
							if (rst.msg == 'SUCCESS') {
								delete req.session.SendAffiliate_ID;
								res.redirect('/user/search/detail?itemid=' + data.Item_ID + '&Contract_ID=' + data.Contract_ID + '&url=/');
								return;
							} else if (rst.msg == 'Already AFF') {
								delete req.session.SendAffiliate_ID;
								res.redirect('/user/search/detail?itemid=' + data.Item_ID + '&Contract_ID=' + data.Contract_ID + '&url=/');
								return;
							}
						})
					}
 
					console.log(body.result[2])

					var params = {
						title,
						topnav_left,
						topnav_title,
						topnav_right,
						thismode,
						isbottomnav,
						Item_ID: data.Item_ID,
						itempic: body.result[0],
						memberinfo: body.result[1],
						iteminfo: body.result[2],
						supinfo: body.result[3],
						Contract_ID: data.Contract_ID,
						detimg: body.result[4],
						delvinfo: body.result[5],
						itemoption: body.result[6],
						url: req.url
					};

					await render.page(res, req, page, params);
				})
			})
		})
	});

};