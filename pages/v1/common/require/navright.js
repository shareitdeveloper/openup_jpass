

function setRight( url, alarmUrl, myUrl) {
    var topnav_right  = '<div class="row m-0 justify-content-end align-self-center">'
    topnav_right += '<div class="col-topicon p-0">'
    topnav_right += '<span class="icon s-24" onclick="move(\'' + alarmUrl + '\',0);  setRedirect(\'' + url + '\',0)  ">remind</span>'
    topnav_right += '<span class="newalarm invisible">99+</span>'
    topnav_right += '</div>'
    topnav_right += '<div class="col-topicon p-0 ml-2">'
    topnav_right += '<span class="icon s-24" onclick="move(\'' + myUrl + '\',0); setRedirect(\'' + url + '\',0)">myinfo</span>'
    topnav_right += '</div>'
    topnav_right += '</div>'
        
    return topnav_right
}

function setRightUsr(url, alarmUrl) {
    var topnav_right  = '<div class="row m-0 justify-content-end align-self-center">'
    topnav_right += '<div class="col-topicon p-0 mr-1">'
    topnav_right += '<span class="icon s-24" onclick="move(\'/member/notice\',0);  setRedirect(\'/member/notice\',0)  ">remind</span>'
    topnav_right += '<span class="newalarmUsr invisible">99+</span>'
    topnav_right += '</div>'
    topnav_right += '</div>'

    return topnav_right
}

exports.setRight = setRight;
exports.setRightUsr = setRightUsr;