module.exports = function(ver, app,  runmode, iamporter ) {	
	var render = require('../../../../../util/render');
	var request = require('request');
	var path = require('path');
	var page = path.resolve(__dirname, '../view/view');
	var thispath = path.resolve(__dirname, '../').replace(/\\/gi, "/").replace(/\\/gi, "/");
	var originPath = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") ,'')
	var url = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") ,'').replace(/_/gi, "/")
	thispath = path.resolve(__dirname, '../../').replace(/\\/gi, "/"); 
	var thismode = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/")+'/','').replace(/\\/gi, "/"); 

	app.get(url,function(req,res) {
	
		var title = '제이패스 - 페이지 찾을 수 없음';
		var topnav_left = '<i class="icon-before" onclick="move(getRedirectURL(1), true)"></i>';
		var topnav_title = '';
		var topnav_right = '<span class="icon before align-middle" onclick="move(getRedirectURL(1), true)">close</span>';
		var isbottomnav = false;

		var params = {
			title,
			topnav_left,
			topnav_title,
			topnav_right,
			thismode,
			isbottomnav
		};
		render.page(res,req,page,params);
	});
};