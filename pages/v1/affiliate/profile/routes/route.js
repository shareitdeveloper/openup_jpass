const { Console } = require('console');

module.exports = function (ver, app, runmode, iamporter) {
	var render = require('../../../../../util/render');
	var request = require('request');
	var path = require('path');

	var page = path.resolve(__dirname, '../view/view');

	var thispath = path.resolve(__dirname, '../').replace(/\\/gi, "/").replace(/\\/gi, "/");
	var originPath = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") ,'')

	var url = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/"), '').replace(/_/gi, "/")

	thispath = path.resolve(__dirname, '../../').replace(/\\/gi, "/");
	var thismode = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") + '/', '').replace(/\\/gi, "/");

	app.get(url + '/:id', function (req, res) {

		var id = req.params.id;
		var sess = req.session;

		sess.updateSession = true;
 
		var topnav_left = '<span class="icon before align-middle" onclick="move(getRedirectURL(1))">before</span>';
		var title = '';
		var topnav_title = '';
		var topnav_right = '<span class="icon before align-middle" onclick="move(getRedirectURL(1), true)">close</span>';
		var isbottomnav = false;

		var data = new Object();
		var info = new Object();

		var pagename = req.query.pagename;
		
		// console.log(sess.Member_ID);
		
		// 다른사람이 내 프로필을 방문
		if (id != sess.Member_ID) {

			var title = '제이패스 - 프로필';
			var topnav_title = '프로필';

			data.Member_ID = id
			data.username = sess.AccountID
			data.uri = '/api/v1/member/memberinfo'

			let options = {
				uri: 'http://127.0.0.1:3000/api/execute',
				method: 'POST',
				body: data,
				json: true
			};

			request.post(options, function (err, httpResponse, body) {

				if (!body[0][0]) { res.redirect('/affiliate/main'); return; }

				info.Member_ID = id
				info.AccountID = body[0][0].AccountID
				info.MemberName = body[0][0].MemberName
				info.Nickname = body[0][0].Nickname
				info.Phone = body[0][0].Phone

				info.Gender = body[1][0].Gender
				info.Birthday = body[1][0].Birthday
				info.MaritalStatus = body[1][0].MaritalStatus

				info.MemberIntro = body[2][0].MemberIntro
				info.Filename = body[2][0].Filename
				info.Filepath = body[2][0].Filepath
				info.CoverFilename = body[2][0].CoverFilename
				info.CoverFilepath = body[2][0].CoverFilepath

				if (body[0][0].MemberType_CD == 'SUP' || body[0][0].MemberType_CD == 'AFF') {

					info.CompanyName = body[3][0].CompanyName
					info.President = body[3][0].President
					info.BizCode = body[3][0].BizCode
					info.CorpCode = body[3][0].CorpCode
					info.Addr = body[3][0].Addr
					info.AddrEtc = body[3][0].AddrEtc
					info.Postcode = body[3][0].Postcode
					info.BizPhone = body[3][0].BizPhone
					info.BizType = body[3][0].BizType
					info.BizItem = body[3][0].BizItem
					info.Manager = body[3][0].Manager
					info.ManagerPhone = body[3][0].ManagerPhone
					info.ManagerEmail = body[3][0].ManagerEmail
					info.BizType_CD = body[3][0].BizType_CD
					info.Status = body[3][0].Status
				}

				data.uri = '/api/v1/member/checkfriend'

				data.Member_ID = sess.Member_ID
				data.Member_FID = id
				data.FriendType_CD = 'AFF'

				request.post(options, function (err, httpResponse, friend) {
					if (friend.msg == "Available") {
						var affstatus = 'Available'
					} else if (friend.msg == "Already") {
						var affstatus = 'Already'
					} else if (friend.msg == "Await") {
						var affstatus = 'Await'
					}
					data.uri = '/api/v1/affiliate/storelist'

					data.Member_ID = req.params.id
					data.Status = 'VAL'
					request.post(options, function (err, httpResponse, storelist) {

						data.uri = '/api/v1/member/checkaffnum'

						data.Member_ID = req.session.Member_ID

						request.post(options, function (err, httpResponse, affnum) {

							console.log(affnum)

							// SNS 정보 불러오기
							var data = new Object();
							data.Member_ID = id

							data.uri = '/api/v1/member/bizregister' 

							var options = {
								uri: 'http://127.0.0.1:3000/api/execute',
								method: 'POST',
								body: data,
								json: true
							};
							//

							request.post(options, function (err, httpResponse, body) {

								let SNScnt = 0
								let SNSData = {}
								if (body.body[1]) {
									SNScnt = body.body[1].length
									// let SNSData = {} body 값이 있어도 여기다 쓰면 undefined, var SNSData로 선언할때는 동작.
									
									let snsNum = 0
									for (var i=0; i < body.body[1].length; i++) {							
										// CompanyName => common.js fn)setSnsChannel(num) 
										switch( body.body[1][i].CompanyName ) {
											case '기타':  
											snsNum = 0
											break
											case '네이버 블로그':  
											snsNum = 1
											break
											case '인스타그램':  
											snsNum = 2
											break
											case '유튜브':  
											snsNum = 3
											break
											case '페이스북':  
											snsNum = 4
											break
											case '트위터':  
											snsNum = 5
											break
											case '네이버밴드':  
											snsNum = 6
											break
											case '네이버 카페':  
											snsNum = 7
											break
											case '다음 카페':  
											snsNum = 8
											break
											case '카카오 스토리':  
											snsNum = 9
											break
											case '티스토리':  
											snsNum = 10
											break
											default: // 기타
											snsNum = 0
											break
										} 

										SNSData[i] = {	
											CompanyName : body.body[1][i].CompanyName, 
											SNSLinkURL : body.body[1][i].SNSLinkURL,
											SNSImgNum : snsNum  // setSnsChannel (idx, num, ReturnImg) commonjs , (0[idx], snsNum, 'Y', width[int] )
										}						
									}
									// console.log(SNSData)
								}
								var params = {
									title,
									topnav_left,
									topnav_title,
									topnav_right,
									thismode,
									isbottomnav,
									info,
									id,
									friend: friend,
									storelist: storelist.result,
									affstatus,
									affnum,
									SNSData: SNSData,
									SNScnt: SNScnt
								};
								render.page(res, req, page, params);
								return;
							})
						})
					})
				})
			})

		// 내 프로필
		} else {

			var title = '제이패스 - 내프로필';
			var topnav_title = '내프로필';
			
			// SNS정보 불러오기 2021.06.18 이병주
			var data = new Object();
			data.Member_ID = sess.Member_ID

			data.uri = '/api/v1/member/bizregister' 

			var options = {
				uri: 'http://127.0.0.1:3000/api/execute',
				method: 'POST',
				body: data,
				json: true
			};

			request.post(options, function (err, httpResponse, body) {
 
				// SNS 정보 불러오기	
				let SNScnt = 0
				let SNSData = {}
				if (body.body[1]) {
					SNScnt = body.body[1].length
					// let SNSData = {} body 값이 있어도 여기다 쓰면 undefined, var SNSData로 선언할때는 동작.
					
					let snsNum = 0
					for (var i=0; i < body.body[1].length; i++) {							
						// CompanyName => snsiconName 
						switch( body.body[1][i].CompanyName ) {
							case '기타':  
							snsNum = 0
							break
							case '네이버 블로그':  
							snsNum = 1
							break
							case '인스타그램':  
							snsNum = 2
							break
							case '유튜브':  
							snsNum = 3
							break
							case '페이스북':  
							snsNum = 4
							break
							case '트위터':  
							snsNum = 5
							break
							case '네이버밴드':  
							snsNum = 6
							break
							case '네이버 카페':  
							snsNum = 7
							break
							case '다음 카페':  
							snsNum = 8
							break
							case '카카오 스토리':  
							snsNum = 9
							break
							case '티스토리':  
							snsNum = 10
							break
							default: // 기타
							snsNum = 0
							break
						} 

						SNSData[i] = {	
							CompanyName : body.body[1][i].CompanyName, 
							SNSLinkURL : body.body[1][i].SNSLinkURL,
							SNSImgNum : snsNum  // setSnsChannel (idx, num, ReturnImg) commonjs , (0, snsNum, 'Y')
						}						
					}
					// console.log(SNSData)
				}

				var params = {
					title,
					topnav_left,
					topnav_title,
					topnav_right,
					thismode,
					isbottomnav,
					id,
					affnum:'me',
					SNSData: SNSData,
					SNScnt: SNScnt
				};
 
				render.page(res, req, page, params);
				return;
			})
		}

	});
};