const { isBuffer } = require('util');

module.exports = function (ver, app, runmode, iamporter) {
	var render = require('../../../../../util/render');
	var request = require('request');
	var path = require('path');

	var page = path.resolve(__dirname, '../view/view');

	var thispath = path.resolve(__dirname, '../').replace(/\\/gi, "/").replace(/\\/gi, "/");
	var originPath = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") ,'')

	var url = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/"), '').replace(/_/gi, "/")

	thispath = path.resolve(__dirname, '../../').replace(/\\/gi, "/");
	var thismode = thispath.replace(path.resolve(__dirname, '../../../').replace(/\\/gi, "/") + '/', '').replace(/\\/gi, "/");

	app.get(url + '/:id', function (req, res) {
		/**
		 * 2021.05.28 고금필 : req.params.id 숫자이외 문자 처리 정규식 추가
		 *
		 */
		var id = req.params.id.replace(/[^0-9]/g,"");
		var sess = req.session;
		
		var title = '';
		// var topnav_title = '';

		/**
		 * 	2021.06.02 고금필 : 파라미터 값 DB디자인 자료형 최대크기 제한 예외처리
		 * 
		 */ 
		if(id <= -2147483648 || 2147483648 <= id) {
			res.redirect('/');
			return;
		}

		// default topnav_title
		const defaultImg = 'logo_default.png'
		
		let titlePart1 = '<img id="main_title_jpass" src="/img/default/logo/'
		
		let titlePart2 = defaultImg
		if(req.query.RegisterWebsite_AID) { // 도메인이 있는경우 
			titlePart2 = 'logo_domainStore_default.jpg'
		} 
		
		let titlePart3 = '" onclick="move(\'/\')" width="100px" style="max-height:49px;">'
		let topnav_title = titlePart1+titlePart2+titlePart3

		var topnav_left = '<span class="icon before align-middle" onclick="move(\'/\')">before</span>';
		var topnav_right = '';
		
		var alarmUrl = '/member/notice'
		var myUrl = '/affiliate/mypage' // 해당 router에 모드별 mypage로 보내는 렌더링에 대한 분기처리되있으므로 여기서는 따로처리하지않음
		var alarm = require('../../../common/require/navright');
		var topnav_right = alarm.setRight(url, alarmUrl, myUrl)
		
		var isbottomnav = false;

		var data = new Object();
		var info = new Object();
		
		// console.log(sess.Member_ID);
		if (id != sess.Member_ID) {

			title = '중개자 스토어';
			// topnav_title = '중개자 스토어';

			data.Member_ID = id
			data.username = sess.AccountID
			data.uri = '/api/v1/member/memberinfo'

			console.log(data)

			let options = {
				uri: 'http://127.0.0.1:3000/api/execute',
				method: 'POST',
				body: data,
				json: true
			};

			/**
			 * 2021.05.28 고금필 : 주석 추가, API return value 정리
			 *
			 * /api/v1/member/memberinfo request.post 실행
			 *
			 * [result info]
			 * body[0][0] - 계정정보
			 * body[1][0] - 개인정보
			 * body[2][0] - 프로필정보
			 * body[3][0] - 기업정보
			 ** body[4][0] - 신분증사본
			 ** body[4][1] - 통장사본
			 ** body[4][2] - 사업자등록증사본
			 ** body[4][3] - 법인인감증면서사본 
			 * body[5][0] - SNS정보 <- 현재 추가되지않음
			 * body[6][0] - 계좌정보
			 * body[7][0] - 스토어이미지정보 2021.09.17 이병주  -- sbb_StoreManagement (pram.ManageType = 'img', pram.EventType = 'read')
			 */
			request.post(options, function (err, httpResponse, body) {

				/**
				 * 2021.05.28 고금필 : 기업정보 입력 안되있는 멤버의 스토어에 접속 시도시 에러발생 예외처리 추가
				 ** 에러발생 원인 - tbBizInfo테이블에 레코드가 없는 사용자의 Member_ID를 URL에서 변경후 접근시 에러발생
				 *
				 * 공급자, 중개자 신청만해도 tbBizInfo테이블에 레코드가 생기기때문에 해당스토어 Member_ID의 MemberType_CD가 USR일경우 메인화면으로 Redirect시킨다.
				 * 또한 MemberType_CD가 공급자 또는 중개자인경우에도 tbBizInfo테이블에 레코드가 없는경우가 있음으로 레코드가 없을경우에도 메인화면으로 Redirect시킨다.
				 */
				if (!body[0][0] || !body[3][0] || body[0][0].MemberType_CD == 'USR') { res.redirect('/'); return; }

				if ( body[7].length > 0 ) {   
					if (body[7][0].LogoName == defaultImg) {
						body[7][0].LogoName = titlePart2
					}
					// topnav_title =  '<img id="main_title_jpass" src="'+body[7][0].LogoPath+body[7][0].LogoName+'" onclick="move(\'/\')" width="100px" style="max-height:49px;"></img>'
				}

				// 제이패스 일떈 그냥 default logo 출력으로변경 2021.10.04
				topnav_title = (req.query.RegisterWebsite_AID) ? '<img id="main_title_jpass" src="'+body[7][0].LogoPath+body[7][0].LogoName+'" onclick="move(\'/\')" width="100px" style="max-height:49px;"></img>' : titlePart1+defaultImg+titlePart3
				
				info.Member_ID = id
				info.AccountID = body[0][0].AccountID
				info.MemberName = body[0][0].MemberName
				info.Nickname = body[0][0].Nickname
				info.Phone = body[0][0].Phone

				info.Gender = body[1][0].Gender
				info.Birthday = body[1][0].Birthday
				info.MaritalStatus = body[1][0].MaritalStatus
				info.MemberIntro = body[2][0].MemberIntro

				// 특정등급 이상만 스토어 이미지 사용 가능
				var common_CD = require('../../../../../util/common_CD');
				const storeManagement_Auth_CD = common_CD.get_CD('storeManagement_Auth_CD') //  array
				if(storeManagement_Auth_CD.includes( body[0][0].MemberGrade_CD) ) {
					info.Filename = body[7][0].Filename
					info.Filepath = body[7][0].Filepath
					info.CoverFilename = body[7][0].CoverFilename
					info.CoverFilepath = body[7][0].CoverFilepath
				}else {
					info.Filename = body[2][0].Filename
					info.Filepath = body[2][0].Filepath
					info.CoverFilename = body[2][0].CoverFilename
					info.CoverFilepath = body[2][0].CoverFilepath
				}
				
				/**
				 * 2021.05.28 고금필 : 사용자가 접근한 스토어의 Member_ID가 유저일경우 접근차단 추가로인해 해당코드는 의미가없음으로 주석처리
				 */
				//if (body[0][0].MemberType_CD == 'SUP' || body[0][0].MemberType_CD == 'AFF') {
				info.CompanyName = body[3][0].CompanyName
				info.President = body[3][0].President
				info.BizCode = body[3][0].BizCode
				info.CorpCode = body[3][0].CorpCode
				info.Addr = body[3][0].Addr
				info.AddrEtc = body[3][0].AddrEtc
				info.Postcode = body[3][0].Postcode
				info.BizType = body[3][0].BizType
				info.BizItem = body[3][0].BizItem
				info.Manager = body[3][0].Manager
				info.ManagerPhone = body[3][0].ManagerPhone
				info.ManagerEmail = body[3][0].ManagerEmail
				info.BizType_CD = body[3][0].BizType_CD
				info.Status = body[3][0].Status

					// console.log(info)
					// console.log('info console')
				//}

				data.uri = '/api/v1/member/checkfriend'
				data.Member_ID = sess.Member_ID
				data.Member_FID = id
				if(sess.MemberMode_CD === 'USR') {
					data.FriendType_CD = 'PLU'
				}else{
					data.FriendType_CD = 'AFF'
				}

				request.post(options, function (err, httpResponse, friend) {
					// 제휴 신청 가능
					if (friend.msg == "Available") {
						var affstatus = 'Available'
					// 이미 제휴 완료 
					} else if (friend.msg == "Already") {
						var affstatus = 'Already'
					// 제휴 신청 중
					} else if (friend.msg == "Await") {
						var affstatus = 'Await'
					// 제휴 신청 받은 상태
					} else if (friend.msg == "Wait") {
						// (20210622 유효경 추가) 내가 제휴 신청 받은 경우
						// (20210622 유효경) 현재 friend.msg 값을 못 받고 있음
						var affstatus = 'Wait'
					}
 
					data.uri = '/api/v1/affiliate/storelist'

					data.Member_ID = id
					data.Status = 'VAL'

					request.post(options, function (err, httpResponse, storelist) {

						// console.log(storelist.result)

						data.uri = '/api/v1/member/checkaffnum'
						data.Member_ID = id

						request.post(options, function (err, httpResponse, affnum) {

							// console.log(topnav_title)
							
							// console.log(affnum)

							var params = {
								title,
								topnav_left,
								topnav_title,
								topnav_right,
								thismode,
								isbottomnav,
								info,
								id,
								friend: friend,
								storelist: storelist.result,
								affstatus,
								affnum,

							};
							render.page(res, req, page, params);
							return;
						})
					})
				})
			})

		} else {

			title = '내스토어';
			// topnav_title = '내스토어';

			data.Member_ID = id
			data.username = sess.AccountID
			data.uri = '/api/v1/member/memberinfo'

			let options = {
				uri: 'http://127.0.0.1:3000/api/execute',
				method: 'POST',
				body: data,
				json: true
			};

			/**
			 * 2021.05.28 고금필 : 주석 추가, API return value 정리
			 *
			 * /api/v1/member/memberinfo request.post 실행
			 *
			 * [result info]
			 * body[0][0] - 계정정보
			 * body[1][0] - 개인정보
			 * body[2][0] - 프로필정보
			 * body[3][0] - 기업정보
			 * body[4][0] - 신분증사본
			 * body[4][1] - 통장사본
			 * body[4][2] - 사업자등록증사본
			 * body[4][3] - 법인인감증면서사본
			 * body[5][0] - SNS정보 <- 현재 추가되지않음
			 * body[6][0] - 계좌정보
			 * body[7][0] - 스토어이미지정보 2021.09.17 이병주  -- sbb_StoreManagement (pram.ManageType = 'img', pram.EventType = 'read')
			 */
			request.post(options, function (err, httpResponse, body) {

				/**
				 * 2021.05.28 고금필 : 접근한 스토어의 Member_ID가 USR일경우 메인화면으로 Redirect
				 */
				if (!body[0][0] || !body[3][0] || body[0][0].MemberType_CD == 'USR') { res.redirect('/'); return; }

				if ( body[7].length > 0 ) {   
					if (body[7][0].LogoName == defaultImg) {
						body[7][0].LogoName = titlePart2
					}
				}

				// 제이패스 일떈 그냥 default logo 출력으로변경 2021.10.04
				topnav_title = (req.query.RegisterWebsite_AID) ? '<img id="main_title_jpass" src="'+body[7][0].LogoPath+body[7][0].LogoName+'" onclick="move(\'/\')" width="100px" style="max-height:49px;"></img>' : titlePart1+defaultImg+titlePart3
			

				info.Member_ID = id
				info.AccountID = body[0][0].AccountID
				info.MemberName = body[0][0].MemberName
				info.Nickname = body[0][0].Nickname
				info.Phone = body[0][0].Phone

				info.Gender = body[1][0].Gender
				info.Birthday = body[1][0].Birthday
				info.MaritalStatus = body[1][0].MaritalStatus

				info.MemberIntro = body[2][0].MemberIntro

				// 특정등급 이상만 스토어 이미지 사용 가능
				var common_CD = require('../../../../../util/common_CD');
				const storeManagement_Auth_CD = common_CD.get_CD('storeManagement_Auth_CD') //  array
				if(storeManagement_Auth_CD.includes( body[0][0].MemberGrade_CD) ) {
					info.Filename = body[7][0].Filename
					info.Filepath = body[7][0].Filepath
					info.CoverFilename = body[7][0].CoverFilename
					info.CoverFilepath = body[7][0].CoverFilepath
				}else {
					info.Filename = body[2][0].Filename
					info.Filepath = body[2][0].Filepath
					info.CoverFilename = body[2][0].CoverFilename
					info.CoverFilepath = body[2][0].CoverFilepath
				}

				/**
				 * 2021.05.28 고금필 : 사용자가 접근한 스토어의 Member_ID가 유저일경우 접근차단 추가로인해 해당코드는 의미가없음으로 주석처리
				 */
				//if (body[0][0].MemberType_CD == 'SUP' || body[0][0].MemberType_CD == 'AFF') {
				info.CompanyName = body[3][0].CompanyName
				info.President = body[3][0].President
				info.BizCode = body[3][0].BizCode
				info.CorpCode = body[3][0].CorpCode
				info.Addr = body[3][0].Addr
				info.AddrEtc = body[3][0].AddrEtc
				info.Postcode = body[3][0].Postcode
				info.BizType = body[3][0].BizType
				info.BizItem = body[3][0].BizItem
				info.Manager = body[3][0].Manager
				info.ManagerPhone = body[3][0].ManagerPhone
				info.ManagerEmail = body[3][0].ManagerEmail
				info.BizType_CD = body[3][0].BizType_CD
				info.Status = body[3][0].Status
				//}

				// console.log(info)
				// console.log('info console')

				data.uri = '/api/v1/member/checkfriend'

				data.Member_ID = sess.Member_ID
				data.Member_FID = id
				data.FriendType_CD = 'AFF'
				data.Status = 'VAL'
			
				data.uri = '/api/v1/affiliate/storelist'

				data.Member_ID = id
				request.post(options, function (err, httpResponse, storelist) {

					// console.log(topnav_title)
					

					var params = {
						title,
						topnav_left,
						topnav_title,
						topnav_right,
						thismode,
						isbottomnav,
						info,
						id,
						friend: '',
						storelist: storelist.result,
						affstatus: '',
						affnum: 'me'
					};
					render.page(res, req, page, params);
					return;
				})
			})

			// var params = {
			// 	title,
			// 	topnav_left,
			// 	topnav_title,
			// 	topnav_right,
			// 	thismode,
			// 	isbottomnav,
			// 	id,
			// 	affnum:'me'
			// };

			// render.page(res, req, page, params);
			// return;

		}
	});
};